﻿using System;
using System.Windows.Forms;
using SoftwareLicense;

namespace _2CQR
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]

        static void Main()
        {

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            if (new DESLoadStore().LoadConfiguration())
            {
                if (DC.dAHolder[(int)DC.dAName.Mode] == " ")
                {
                    Application.Run(new FormMainL());
                }
                LicCheck lic = new LicCheck();
                lic.SoftwareNo = 9;
                lic.SoftwareTypeDisplay = "2CQR_RFDetect";
                lic.PathWorkingDirectory = @"C:\2CQR\RFDetect\";
                lic.SInfo = "C003012AB";
                new Selectfunc().selectFunc();
                if (lic.LicChecker() == true)
                {
                    Application.Run(new Form1());
                    lic.LastStsCheck();
                }
            }
            else
                MessageBox.Show("Error in Configuration File");
        }
    }
}