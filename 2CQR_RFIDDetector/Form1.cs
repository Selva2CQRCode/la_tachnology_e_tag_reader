﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _2CQR
{
    public partial class Form1 : Form
    {
        #region Declaration
        private System.Windows.Forms.Timer timerMain = new System.Windows.Forms.Timer();

        private FormSet formset;
        private static int prevColor = 0;
        private static int alarmFirst = 0;
        private static int sno = 0;
        private static int count = 0;
        private static bool oncedonesync = false;

        #endregion

        // Public constructor
        public Form1()
        {
            InitializeComponent();
            DoItFirst();
            // check("504C30303100000000000000");
         //   Call_Service();
        }

        private void Call_Service()
        {
            SelvaWebService selvaWebService = new SelvaWebService();
            selvaWebService.update("PL001," + DateTime.Now + ",192.168.0.251");
        }

        private void check(string hexString)
        {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < hexString.Length; i += 2)
            {
                string hs = hexString.Substring(i, 2);
                sb.Append(Convert.ToChar(Convert.ToUInt32(hs, 16)));
            }
            MessageBox.Show(sb.ToString());
        }

        public void DoItFirst()
        {
            this.Text = DC.dAHolder[(int)DC.dAName.DisplayText];


            DC.formTriggerTmr.Interval = 2;
            DC.formTriggerTmr.Tick += new EventHandler(this.formTriggerTmr_Tick);
            new Connectivity().initTimers();
            timerMain.Interval = Int32.Parse(DC.dAHolder[(int)DC.dAName.Delay]) * 1000;
            timerMain.Tick += TimerMain_Tick;
            DC.timerAlarm.Interval = 1;
            DC.timerAlarm.Tick += timerAlarm_Tick;
            if (DC.dAHolder[(int)DC.dAName.CountDP] == "Yes")
            {
                this.labelCout.Visible = true;
                this.labelCout.Text = "0";
                this.buttonClear.Visible = true;
            }
        }

        private void formTriggerTmr_Tick(object sender, EventArgs e)
        {
            try
            {
                string sts = "-";
                int color = 1;
                if (DC.rfidfirsttime == false)
                {
                    string ipAddr = DC.dAHolder[(int)DC.dAName.ReaderIP].Split(':')[0];
                    string port = "";
                    try { port = DC.dAHolder[(int)DC.dAName.ReaderIP].Split(':')[1]; } catch { }
                    sts = DC.readerTypeSel.initRfidReader(ipAddr, port, Handle);
                    if (sts == "Success")
                    {
                        DC.rfidfirsttime = true;
                        timerMain.Start();
                    }
                    else if (sts != "-")
                    {
                        color = 3;
                    }
                }
                else
                {

                }
                if (DC.connInitialized == false)
                {
                    color = 2;
                }
                if (prevColor != color)
                {
                    if (color == 1)
                        this.BackColor = Color.Green;
                    else if (color == 2)
                        this.BackColor = Color.Orange;
                    else
                        this.BackColor = Color.Red;
                    prevColor = color;
                }
                DC.formTriggerTmr.Stop();
            }
            catch (Exception ex)
            {
                DC.ErrorLogFile(DC.ERRFILELOC, "EFO1050:" + ex.ToString());
            }
        }

        private void TimerMain_Tick(object sender, EventArgs e)
        {
            timerMain.Stop();
            if (DC.dAHolder[(int)DC.dAName.Synctime] != " ")
            {
                if (DateTime.Now.ToString("HH") == DC.dAHolder[(int)DC.dAName.Synctime])
                {
                    if (oncedonesync == false)
                    {
                        DC.connTypeSel.postSyncTotalData();
                        oncedonesync = true;
                    }
                }
                else
                {
                    oncedonesync = false;
                }
            }
            // Check if any tag is detected
            if (DC.readerTypeSel.tagDetection())
            {
                // Read the Tag ID
                string Item;
                while ((Item = DC.readerTypeSel.tagRead()) != "")
                {
                    if (DC.updateTypeSel.update(Item))
                    {
                        DC.ErrorLogFile("ReportLogs//RepLog" + DateTime.Now.ToString("yyyy_MM") + ".csv", Item);
                    }
                    else
                    {
                        DC.NoDateErrorLogFile("aer.txt", Item);
                    }
                }
            }
            try
            {
                string[] tempSync = System.IO.File.ReadAllLines("aer.txt");
                if (tempSync.Length > 0)
                {
                    System.IO.File.Delete("aer.txt");
                    for (int i = 0; i < tempSync.Length; i++)
                    {
                        if (DC.updateTypeSel.update(tempSync[i]))
                        {
                            DC.ErrorLogFile("ReportLogs//RepLog" + DateTime.Now.ToString("yyyy_MM") + ".csv", tempSync[i]);
                        }
                        else
                        {
                            DC.NoDateErrorLogFile("aer.txt", tempSync[i]);
                        }
                    }
                }
            }
            catch
            {

            }
            timerMain.Start();
        }


        private void timerAlarm_Tick(object sender, EventArgs e)
        {
            if (alarmFirst == 0)
            {
                DC.timerAlarm.Stop();
                alarmFirst = 1;
                DC.timerAlarm.Interval = 5000;
                DC.timerAlarm.Start();
            }
            else if (alarmFirst == 1)
            {
                DC.timerAlarm.Stop();
                alarmFirst = 0;
                DC.timerAlarm.Interval = 1;
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }


        private void textBoxPassword_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (this.textBoxPassword.Text == "adminset")
                {
                    if (formset != null)
                    {
                        formset.Close();
                    }
                    formset = new FormSet();
                    formset.Show();
                }
                else if (this.textBoxPassword.Text == "sync" || this.textBoxPassword.Text == "sync2" || this.textBoxPassword.Text == "sync30" || this.textBoxPassword.Text == "syncall")
                {
                    Cursor cursor = Cursors.WaitCursor;
                    if (DC.readerTypeSel.getSyncTotalData(this.textBoxPassword.Text))
                    {
                        DC.mode = this.textBoxPassword.Text;
                        if (DC.connTypeSel.postSyncTotalData())
                        {
                            MessageBox.Show("Sync Success!");
                        }
                        else
                        {
                            MessageBox.Show("Sync Failed! DB Update Fail!");
                        }
                    }
                    else
                        MessageBox.Show("Sync Failed! Pull from reader Fail!");
                    cursor = Cursors.Default;
                }
                this.textBoxPassword.Clear();
                this.textBoxPassword.Visible = false;
            }
        }

        private void labelSettings_Click(object sender, EventArgs e)
        {
            this.textBoxPassword.Clear();
            this.textBoxPassword.Visible = true;
            this.textBoxPassword.Focus();
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            timerMain.Stop();
            DC.readerTypeSel.closeRfidReader();
        }

        private void buttonClear_Click(object sender, EventArgs e)
        {
            count = 0;
            this.labelCout.Text = "0";
        }
    }
}
