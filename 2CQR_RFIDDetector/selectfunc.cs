﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _2CQR
{
    public class Selectfunc
    {
        public void selectFunc()
        {
            switch (DC.dAHolder[(int)DC.dAName.ReaderTypeN])
            {
               
                case "C001":
                    DC.readerTypeSel = new C001();
                    break;
                case "C002":
                    DC.readerTypeSel = new C002();
                    break;
                case "C003":
                    DC.readerTypeSel = new C003();
                    break;
                case "Demo":
                    DC.readerTypeSel = new DemoReader();
                    break;
                case "C006":
                    DC.readerTypeSel = new C006();
                    break;
               
              
                case "2C003":
                    DC.readerTypeSel = new _2C003();
                    break;
                default:
                    DC.readerTypeSel = new C003();
                    break;
            }
            switch (DC.dAHolder[(int)DC.dAName.Output])
            {
                case "Cursor":
                    DC.updateTypeSel = new CursorUpdate();
                    break;
                case "DB":
                    DC.updateTypeSel = new DMSUpdate();
                    break;
                case "API":
                    DC.updateTypeSel = new DMSUpdate();
                    break;
                default:
                    DC.updateTypeSel = new CursorUpdate();
                    break;
            }
            switch (DC.dAHolder[(int)DC.dAName.Comm])
            {
                case "Demo":
                    DC.connTypeSel = new Demo();
                    break;
                case "RaDB":
                    DC.connTypeSel = new RasiWebService();
                    break;
                case "SVWs":
                    DC.connTypeSel = new SVasaviWebServiceSOAP();
                    break;
                case "SDB":
                    DC.connTypeSel = new SelvaWebService();
                    break;
                default:
                    DC.connTypeSel = new Demo();
                    break;
            }
            switch (DC.dAHolder[(int)DC.dAName.DataModel])
            {
                case "Default":
                    DC.dataModelTypeSel = new DefFormat();
                    break;
                case "ASCII":
                    DC.dataModelTypeSel = new ASCII();
                    break;
                case "ASCIISV":
                    DC.dataModelTypeSel = new ASCIISV();
                    break;
                default:
                    DC.dataModelTypeSel = new DefFormat();
                    break;
            }
        }
    }
}
