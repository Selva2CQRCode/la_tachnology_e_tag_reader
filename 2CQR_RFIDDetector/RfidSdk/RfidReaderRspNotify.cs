﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RfidSdk
{
    public interface RfidReaderRspNotify
    {
        void OnRecvResetRsp(RfidReader reader,byte result);
        void OnRecvSetFactorySettingRsp(RfidReader reader, byte result);
        void OnRecvStartInventoryRsp(RfidReader reader, byte result);
        void OnRecvStopInventoryRsp(RfidReader reader, byte result);
        void OnRecvDeviceInfoRsp(RfidReader reader, byte[] firmwareVersion,byte deviceType);
        void OnRecvSetWorkParamRsp(RfidReader reader, byte result);

        void OnRecvQueryWorkParamRsp(RfidReader reader, byte result, RfidWorkParam workParam);

        void OnRecvSetTransmissionParamRsp(RfidReader reader, byte result);

        void OnRecvQueryTransmissionParamRsp(RfidReader reader, byte result, RfidTransmissionParam transmissiomParam);

        void OnRecvSetAdvanceParamRsp(RfidReader reader, byte result);

        void OnRecvQueryAdvanceParamRsp(RfidReader reader, byte result, RfidAdvanceParam advanceParam);

        void OnRecvTagNotify(RfidReader reader, TlvValueItem[] tlvItems, byte tlvCount);
    }
}
