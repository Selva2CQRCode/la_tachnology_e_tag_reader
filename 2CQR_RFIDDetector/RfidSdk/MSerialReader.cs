﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RfidSdk
{
    public class MSerialReader:RfidReader
    {
        RfidReaderRspNotify _notifyImpl;
        private const int Message_Len_Start_Pos = 6;
        private const int Message_Frame_Code_Pos = 5;
        private const int Message_Attr_Start_Pos = 8;


        public MSerialReader(RfidReaderRspNotify notifyImpl)
        {
            this._notifyImpl = notifyImpl;
            transport = new Transport(this);
        }
        
        private int GetPositionByAttrType(byte[] message,int messageLen,byte attrType)
        {
            int attrPos = -1;
            int attrLen = 0;
            int curPos = 0;
            attrLen = message[Message_Len_Start_Pos];
            attrLen = attrLen << 8;
            attrLen += message[Message_Len_Start_Pos + 1];

            curPos = Message_Attr_Start_Pos;
            while(curPos < (Message_Attr_Start_Pos + attrLen))
            {
                if (message[curPos] == attrType)
                {
                    attrPos = curPos;
                    break;
                }
                else
                {
                    curPos = curPos + message[curPos+1] + 2;
                }
            }
            return attrPos;
        }

        public override void OnRecvCompleted(byte[] messageData,int messageLen)
        {
            int statusPos = 0;
            if (null == _notifyImpl)
            {
                return;
            }           
            if (messageData[2] == 0x01)
            {
                //if the message is a response
                switch (messageData[Message_Frame_Code_Pos])
                {
                    case (byte)Frame_Code.FRAME_CODE_RESET:
                        statusPos = GetPositionByAttrType(messageData,messageLen, (byte)Tlv_Attr_Code.TLV_ATTR_CODE_STATUS);
                        if (statusPos > 0)
                        {
                            _notifyImpl.OnRecvResetRsp(this,messageData[statusPos+2]);
                        }
                        else
                        {
                            _notifyImpl.OnRecvResetRsp(this, 0xFE);
                        }
                        break;
                    case (byte)Frame_Code.FRAME_CODE_SET_DEFAULT_PARAM:
                        statusPos = GetPositionByAttrType(messageData, messageLen, (byte)Tlv_Attr_Code.TLV_ATTR_CODE_STATUS);
                        if (statusPos > 0)
                        {
                            _notifyImpl.OnRecvSetFactorySettingRsp(this, messageData[statusPos + 2]);
                        }
                        else
                        {
                            _notifyImpl.OnRecvSetFactorySettingRsp(this, 0xFE);
                        }
                        break;
                    case (byte)Frame_Code.FRAME_CODE_START_INVENTORY:
                        statusPos = GetPositionByAttrType(messageData, messageLen, (byte)Tlv_Attr_Code.TLV_ATTR_CODE_STATUS);
                        if (statusPos > 0)
                        {
                            _notifyImpl.OnRecvStartInventoryRsp(this, messageData[statusPos + 2]);
                        }
                        else
                        {
                            _notifyImpl.OnRecvStartInventoryRsp(this, 0xFE);
                        }
                        break;
                    case (byte)Frame_Code.FRAME_CODE_STOP_INVENTORY:
                        statusPos = GetPositionByAttrType(messageData, messageLen, (byte)Tlv_Attr_Code.TLV_ATTR_CODE_STATUS);
                        if (statusPos > 0)
                        {
                            _notifyImpl.OnRecvStopInventoryRsp(this, messageData[statusPos + 2]);
                        }
                        else
                        {
                            _notifyImpl.OnRecvStopInventoryRsp(this, 0xFE);
                        }
                        break;
                    case (byte)Frame_Code.FRAME_CODE_QUERY_DEVICE_INFO:
                        byte[] version = null;
                        statusPos = GetPositionByAttrType(messageData, messageLen, (byte)Tlv_Attr_Code.TLV_ATTR_CODE_STATUS);
                        int versionPos = GetPositionByAttrType(messageData, messageLen, (byte)Tlv_Attr_Code.TLV_ATTR_FIRMWARE_VERSION);
                        int typePos = GetPositionByAttrType(messageData, messageLen, (byte)Tlv_Attr_Code.TLV_ATTR_FIRMWARE_VERSION);
                        if (versionPos > 0)
                        {
                            version = new byte[messageData[versionPos + 1]];
                            Array.Copy(messageData, versionPos + 2, version, 0, messageData[versionPos + 1]);
                            
                        }
                        _notifyImpl.OnRecvDeviceInfoRsp(this, version, messageData[typePos + 2]);
                        break;
                    case (byte)Frame_Code.FRAME_CODE_SET_WORKING_PARAM:
                        statusPos = GetPositionByAttrType(messageData, messageLen, (byte)Tlv_Attr_Code.TLV_ATTR_CODE_STATUS);
                        if (statusPos > 0)
                        {
                            _notifyImpl.OnRecvSetWorkParamRsp(this, messageData[statusPos + 2]);
                        }
                        else
                        {
                            _notifyImpl.OnRecvSetWorkParamRsp(this, 0xFE);
                        }
                        break;
                    case (byte)Frame_Code.FRAME_CODE_QUERY_WORKING_PARAM:
                        HandleQueryWorkingParamRsp(messageData, messageLen);
                        break;
                    case (byte)Frame_Code.FRAME_CODE_SET_TRANSPORT_PARAM:
                        statusPos = GetPositionByAttrType(messageData, messageLen, (byte)Tlv_Attr_Code.TLV_ATTR_CODE_STATUS);
                        if (statusPos > 0)
                        {
                            _notifyImpl.OnRecvSetTransmissionParamRsp(this, messageData[statusPos + 2]);
                        }
                        else
                        {
                            _notifyImpl.OnRecvSetTransmissionParamRsp(this, 0xFE);
                        }
                        break;
                    case (byte)Frame_Code.FRAME_CODE_QUERY_TRANSPORT_PARAM:
                        HandleQueryTransmissionParamRsp(messageData, messageLen);
                        break;
                    case (byte)Frame_Code.FRAME_CODE_SET_ADVANCE_PARAM:
                        statusPos = GetPositionByAttrType(messageData, messageLen, (byte)Tlv_Attr_Code.TLV_ATTR_CODE_STATUS);
                        if (statusPos > 0)
                        {
                            _notifyImpl.OnRecvSetTransmissionParamRsp(this, messageData[statusPos + 2]);
                        }
                        else
                        {
                            _notifyImpl.OnRecvSetTransmissionParamRsp(this, 0xFE);
                        }
                        break;
                    case (byte)Frame_Code.FRAME_CODE_QUERY_ADVANCE_PARAM:
                        HandleQueryAdvanceParamRsp(messageData, messageLen);
                        break;
                }
            }
            else if (messageData[2] == 0x02)
            {
                //if the message is a notify.like upload tag data
                switch(messageData[5])
                {
                    case 0x80:
                        HandleRecvTagNotify(messageData, messageLen);
                        break;
                }
            }
        }

        private void HandleRecvTagNotify(byte[] message,int messageLen)
        {
            TlvValueItem[] tlvItems = new TlvValueItem[6];
            byte tlvCount = 0;
            int curPos = 0;
            int paramLen = 0;
            int maxPos = 0;
            int subPos = 0;
            paramLen = message[Message_Len_Start_Pos];
            paramLen = paramLen << 8;
            paramLen = paramLen + message[Message_Len_Start_Pos + 1];
            curPos = Message_Attr_Start_Pos;
            maxPos = curPos + paramLen;

            while (curPos < maxPos)
            {
                if ((byte)Tlv_Attr_Code.TLV_ATTR_CODE_SINGLE_TAG_DATA == message[curPos])
                {
                    subPos = curPos + 2;
                    while(subPos < (curPos+2+message[curPos+1]))
                    {
                        tlvItems[tlvCount] = new TlvValueItem();
                        tlvItems[tlvCount]._tlvType = message[subPos];
                        tlvItems[tlvCount]._tlvLen = message[subPos + 1];
                        tlvItems[tlvCount]._tlvValue = new byte[message[subPos + 1]];
                        Array.Copy(message, subPos + 2, tlvItems[tlvCount]._tlvValue, 0, message[subPos + 1]);
                        tlvCount++;
                        subPos = subPos + message[subPos + 1] +2;
                    }
                   
                }
                curPos = curPos + message[curPos+1] + 2;
            }
            _notifyImpl.OnRecvTagNotify(this, tlvItems,tlvCount);
        }

        private void HandleQueryWorkingParamRsp(byte[] message,int messageLen)
        {
            int statusPos = GetPositionByAttrType(message, messageLen, (byte)Tlv_Attr_Code.TLV_ATTR_CODE_STATUS);
            int workParamPos = GetPositionByAttrType(message, messageLen, (byte)Tlv_Attr_Code.TLV_ATTR_WORKING_PARAM);
            if (statusPos < 0)
            {
                _notifyImpl.OnRecvQueryWorkParamRsp(this, 0xFE, null);
                return;
            }
            else if (message[statusPos+2] != 0)
            {
                _notifyImpl.OnRecvQueryWorkParamRsp(this, message[statusPos + 2], null);
                return;
            }

            if (workParamPos < 0)
            {
                _notifyImpl.OnRecvQueryWorkParamRsp(this, 0xFE, null);
                return;
            }

            RfidWorkParam param = new RfidWorkParam();
            param.SetParamFromMessage(message, workParamPos+2);
            _notifyImpl.OnRecvQueryWorkParamRsp(this, 0, param);

        }

        private void HandleQueryTransmissionParamRsp(byte[] message, int messageLen)
        {
            int statusPos = GetPositionByAttrType(message, messageLen, (byte)Tlv_Attr_Code.TLV_ATTR_CODE_STATUS);
            int transmissionParamPos = GetPositionByAttrType(message, messageLen, (byte)Tlv_Attr_Code.TLV_ATTR_TRANSPORT_PARAM);
            if (statusPos < 0)
            {
                _notifyImpl.OnRecvQueryTransmissionParamRsp(this, 0xFE, null);
                return;
            }
            else if (message[statusPos + 2] != 0)
            {
                _notifyImpl.OnRecvQueryTransmissionParamRsp(this, message[statusPos + 2], null);
                return;
            }

            if (transmissionParamPos < 0)
            {
                _notifyImpl.OnRecvQueryTransmissionParamRsp(this, 0xFE, null);
                return;
            }

            RfidTransmissionParam param = new RfidTransmissionParam();
            param.SetParamFromMessage(message, transmissionParamPos + 2);
            _notifyImpl.OnRecvQueryTransmissionParamRsp(this, 0, param);
        }

        private void HandleQueryAdvanceParamRsp(byte[] message, int messageLen)
        {
            int statusPos = GetPositionByAttrType(message, messageLen, (byte)Tlv_Attr_Code.TLV_ATTR_CODE_STATUS);
            int advancePos = GetPositionByAttrType(message, messageLen, (byte)Tlv_Attr_Code.TLV_ATTR_ADVANCE_PARAM);
            if (statusPos < 0)
            {
                _notifyImpl.OnRecvQueryTransmissionParamRsp(this, 0xFE, null);
                return;
            }
            else if (message[statusPos + 2] != 0)
            {
                _notifyImpl.OnRecvQueryTransmissionParamRsp(this, message[statusPos + 2], null);
                return;
            }

            if (advancePos < 0)
            {
                _notifyImpl.OnRecvQueryTransmissionParamRsp(this, 0xFE, null);
                return;
            }

            RfidAdvanceParam param = new RfidAdvanceParam();
            param.SetParamFromMessage(message, advancePos + 2);
            _notifyImpl.OnRecvQueryAdvanceParamRsp(this, 0, param);
        }
        public override void HandleRecvData()
        {
            transport.HandleRecvData();
        }
    }

    enum Frame_Code
    {
        FRAME_CODE_RESET = 0x10,
        FRAME_CODE_SET_DEFAULT_PARAM = 0x12,
        FRAME_CODE_START_INVENTORY = 0x21,
        FRAME_CODE_INVENTORY_ONCE = 0x22,
        FRAME_CODE_STOP_INVENTORY = 0x23,
        FRAME_CODE_WRITE_TAG = 0x30,
        FRAME_CODE_READ_BLOCK = 0x31,
        FRAME_CODE_QUERY_DEVICE_INFO = 0x40,
        FRAME_CODE_SET_WORKING_PARAM = 0x41,
        FRAME_CODE_QUERY_WORKING_PARAM = 0x42,
        FRAME_CODE_QUERY_TRANSPORT_PARAM = 0x43,
        FRAME_CODE_SET_TRANSPORT_PARAM = 0x44,
        FRAME_CODE_QUERY_ADVANCE_PARAM = 0x45,
        FRAME_CODE_SET_ADVANCE_PARAM = 0x46,
        FRAME_CODE_SET_SIGNLE_PARAM = 0x48,
        FRAME_CODE_QUERY_SINGLE_PARAM = 0x49,
        FRAME_CODE_NOTIFY_TAG = 0x80,
        FRAME_CODE_PREPARE_UPDATE = 0xF4
    };

    enum Tlv_Attr_Code
    {
        TLV_ATTR_CODE_IDLE = 0x00, //无效数据格式
        TLV_ATTR_CODE_EPC = 0x01,    //EPC数据
        TLV_ATTR_CODE_USER = 0x02,   //user区的数据
        TLV_ATTR_CODE_RESERVE = 0x03,//保存的数据
        TLV_ATTR_CODE_TEMPERATURE = 0x04,    //温度数据
        TLV_ATTR_CODE_RSSI = 0x05,
        TLV_ATTR_CODE_FLUSH_NOW_TIME = 0x06,
        TLV_ATTR_CODE_STATUS = 0x07,
        TLV_ATTR_TAG_OPERATION_INFO = 0x08,  //mambank addr[2] length[2]
        TLV_ATTR_CODE_PASSWORD = 0x10,
        TLV_ATTR_FIRMWARE_VERSION = 0x20,
        TLV_ATTR_DEVICE_TYPE = 0x21,
        TLV_ATTR_WORKING_PARAM = 0x23,
        TLV_ATTR_TRANSPORT_PARAM = 0x24,
        TLV_ATTR_ADVANCE_PARAM = 0x25,
        TLV_ATTR_SIGNLE_PARAM = 0x26,
        TLV_ATTR_WRITE_TAG = 0x27,
        TLV_ATTR_TAG_READ_TAG = 0x28,
        TLV_ATTR_CODE_SINGLE_TAG_DATA = 0x50
    };
}
