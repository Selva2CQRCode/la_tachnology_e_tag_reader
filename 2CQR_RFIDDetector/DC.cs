﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using Renci.SshNet;
using System.Data;
using System.Net.Sockets;

namespace _2CQR
{
    class DC
    {
        public static int indiviItemPiecePos = 0;
        public static int indivItemNoOfPieces = 0;
        // Count number of bytes to be written in particular DataModel
        public static int DataModelCnt;
        // To indicate if the tag is unprogrammed
        public static byte blankIndicator = 0;
        // For holding checksum
        public static bool checkSumVerify = true;
        // Array to hold the read data stored in the Tag
        public static byte[] tagReadData;
        // To hold the tags Item ID 
        public static char[] mainTagReadDataASCII;
        // Array to hold the read data in the Ascii format
        public static char[] tagReadDataASCII;
        public static int seqNo = -1;
        // To hold the string to be sent
        public static string messSendData = null;
        // For socket Handling(connect,send,release)
        public static Socket sender;
        public static string errMsg = "";
        public static string mode = "";
        public static string UOKfirstReader = "";
        public static int a001cardtype = 0;
        public static string bioSyncTotalData = "";
        public static DataSet ds;
        public static string SCMStatus = "";
        public static bool sqlSts = false;
        public static bool connInitialized = false;
        public static bool enterCriticalArea = false;
        public static bool rfidfirsttime = false;
        public static string ERRFILELOC = "Errlogs\\ErrLog" + DateTime.Now.ToString("yyyy_MM") + ".txt";
        public static string SERVFILELOC = "Serverlogs\\ServerLog" + DateTime.Now.ToString("yyyy_MM") + ".txt";
        public static string EGATEENTRYLOC = "RepLogs\\RepLog" + DateTime.Now.ToString("yyyy_MM") + ".csv";
        public static string UNAUTHLOC = "UnAuthLogs\\ErrLog" + DateTime.Now.ToString("yyyy_MM") + ".csv";
        public static InterReaderTypeSel readerTypeSel;
        public static SqlConnection sqlConnect;
        public static MySqlConnection mySqlConnect;
        public static SshClient mySshClient;
        public static string opString = "";
        public static InterUpdate updateTypeSel;
        public static System.Windows.Forms.Timer formTriggerTmr = new System.Windows.Forms.Timer();
        public static System.Windows.Forms.Timer timerAlarm = new System.Windows.Forms.Timer();
        public static InterConnectivity connTypeSel;
        public static InterDataModel dataModelTypeSel;
        public struct settingsParameter
        {
            public bool sts;
            public byte powerDbm;
            public byte beep;
            public byte antenna;
        }
        public static settingsParameter setPara;

        // Enum to hold the tab contents value of the form settings
        public enum dAName
        {
            ReaderTypeN,
            SIP,SUN, Pass, DB,Location,
            ServerInitialTime, ServerOnlineTime,
            WaitForServerResponse,
            MeansOfComm,
            ReaderIP,Delay,Output,Comm,DataModel,
            C6Ant,SSH,
            EMComPortNo,Buzzer,ReaderOnlineTime,QRep,CountDP,DisplayText,Mode,Synctime,AddDBDetails,
        };
        // Array to hold keys
        public static string[] dAKeys = {"KeyReaderType",
                                                  "KeySIP",
                                                  "KeySUN","KeyPass","KeyDB","KeyLocation",
                                                  "KeyServerInitialTime", "KeyServerOnline",
                                                  "KeyWaitForServerResponse",
                                                  "KeyMeansOfComm",
                                                  "KeyReaderIP","KeyDelay","KeyOutput","KeyComm","KeyDataModel",
                                                  "KeyC6Ant","KeySSH",
                                                  "KeyEMComPortNo","KeyBuzzer","KeyReaderOnlineTime","KeyQRep","KeyCountDP","KeyDisplayText",
                                                  "KeyMode","KeySyncTime","KeyAddDBDetails",
        };

        // To hold the default values of the form contents
        public static string[] dAValue = { "XSml3BSL7Mc=",
                                                   "iO0bi/6WzUS0xZYYC9jwymwd+gS/Nkm5", 
                                                   "84+t5vdu2WseLR2j5uqBRA==","hWN5May3WA+wUq6WVM4XHA==","j1ZsO+0ZJGoztefD0oJWog==","XSml3BSL7Mc=", // "UserName","Password","Database","Location "" "
                                                   "LibKLjb30UE=", "65M+QBNoQ44=", // "1", "15",
                                                   "aQadqv/hp6U=", // "25",
                                                   "OzzwHCsoecc=", //Demo
                                                   "VQfbtAukwqJZK0AmZ4XsYw==" ,//192.168.0.250
                                                   "u+MMi/H5xDU=","cK2L01dsxf4=","OzzwHCsoecc=","ZjI4lQGODd0=",// 3s, Cursor,Demo, Default
                                                   "u+MMi/H5xDU=",// 3 1+ 2antenna(1,2)
                                                   "gE+80tqmGblgosXFrU+FTQ==",//user,pass
                                                   "LibKLjb30UE=",//1 (comport no em)
                                                   "gaoUMXmdK3E=", // "No",
                                                   "O6lEzqehAQQ=", // 1800
                                                   "XSml3BSL7Mc=", //" "
                                                      "gaoUMXmdK3E=", // "No",
                                                      "GD8DGVVjajdgisYwqKQHsQ==",//2CQR_RFID
                                                      "XSml3BSL7Mc=",//" "
                                                      "XSml3BSL7Mc=",// " " synctime
                                                      "XSml3BSL7Mc=",// add db details
        };
        // To hold the actual value in the form
        public static string[] dAHolder = new string[26];
        // To open a log file
        public static void ErrorLogFile(string errFileLoc, string errMsg)
        {
            try
            {
                StreamWriter ErrorLog;
                if (!File.Exists(errFileLoc))
                {
                    ErrorLog = new StreamWriter(errFileLoc);
                }
                else
                {
                    ErrorLog = File.AppendText(errFileLoc);
                }
                // Write the Err Msg in Log
                ErrorLog.WriteLine("( " + DateTime.Now.ToString() + " ) - " + errMsg);
                // Close the Log
                ErrorLog.Close();
            }
            catch
            {
            }
        }

        public static void NoDateErrorLogFile(string errFileLoc, string errMsg)
        {
            try
            {
                StreamWriter ErrorLog;
                if (!File.Exists(errFileLoc))
                {
                    ErrorLog = new StreamWriter(errFileLoc);
                }
                else
                {
                    ErrorLog = File.AppendText(errFileLoc);
                }

                // Write the Err Msg in Log
                ErrorLog.WriteLine(errMsg);
                // Close the Log
                ErrorLog.Close();
            }
            catch
            {
            }
        }

        // To get the response from the server
        public static string GetResponse(string result, string sep)
        {
            // Get the index
            int index = result.IndexOf(sep);
            // If sep present
            if (index >= 0)
            {
                // Leave sep and get the remaining string and return
                index += 3;
                string[] result1 = result.Substring(index).Split('|');
                return result1[0];
            }
            // else
            else
                // return null
                return null;
        }
    }
}
