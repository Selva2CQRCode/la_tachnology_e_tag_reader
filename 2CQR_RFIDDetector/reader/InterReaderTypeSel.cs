﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _2CQR
{
    public interface InterReaderTypeSel
    {
        string initRfidReader(string IPAddr, string port, IntPtr Handle);
        bool tagDetection();
        string tagRead();
        int setPower(string power);
        void closeRfidReader();
        bool getSettings();
        bool setSettings();

        void alarmOn();
        bool onlineReaderChk();

        bool getSyncTotalData(string data);

    }
}
