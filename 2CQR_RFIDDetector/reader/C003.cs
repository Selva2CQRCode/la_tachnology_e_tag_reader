﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UHF;

namespace _2CQR
{
    //Chafon - 4 port reader
    public class C003 : InterReaderTypeSel
    {
        private static byte fComAdr = 0xff; 
        private int fCmdRet = 30;
        private static int frmcomportindex;
        private static bool strtInventoryScan = false;
        private string fInventory_EPC_List = "";
        private static string mainString = "";
        private static string prevSerialnumber = "";
        private static int[] count = new int[500];
        private static System.Windows.Forms.Timer timer_RealTime = new System.Windows.Forms.Timer();

        public bool getSyncTotalData(string data)
        {
            return true;
        }
        public void closeRfidReader()
        {
           try
            {
                if (frmcomportindex > 0)
                    RWDev.CloseNetPort(frmcomportindex);
            }
            catch
            {

            }
        }
       
        public string initRfidReader(string IPAddr, string port, IntPtr Handle)
        {
            try
            {
                string strException = string.Empty;
                string ipAddress = IPAddr;
                int nPort = Convert.ToInt32(port);
                fComAdr = 255;
                int FrmPortIndex = 0;
                fCmdRet = RWDev.OpenNetPort(nPort, ipAddress, ref fComAdr, ref FrmPortIndex);
                if (fCmdRet != 0)
                {
                    DC.ErrorLogFile(DC.ERRFILELOC, "EC003001:" + fCmdRet.ToString());
                    return ("E001:" + fCmdRet.ToString());
                }
                else
                {
                    frmcomportindex = FrmPortIndex;
                    timer_RealTime.Interval = 50;
                    timer_RealTime.Tick += new System.EventHandler(timer_RealTime_Tick);
                    timer_RealTime.Start();
                    return SetMode(1);
                }
            }
            catch (Exception ex)
            {
                DC.ErrorLogFile(DC.ERRFILELOC, "EC003002:" + ex.ToString());
                return ("E002:" + ex.ToString());

            }
        }


        public bool getSettings()
        {
            DC.setPara.sts = false;
            timer_RealTime.Stop();
            SetMode(0);
            byte TrType = 0;
            byte[] VersionInfo = new byte[2];
            byte ReaderType = 0;
            byte ScanTime = 0;
            byte dmaxfre = 0;
            byte dminfre = 0;
            byte powerdBm = 0;
            byte Ant = 0;
            byte BeepEn = 0;
            byte OutputRep = 0;
            byte CheckAnt = 0;
            fCmdRet = RWDev.GetReaderInformation(ref fComAdr, VersionInfo, ref ReaderType, ref TrType, ref dmaxfre, ref dminfre, ref powerdBm, ref ScanTime, ref Ant, ref BeepEn, ref OutputRep, ref CheckAnt, frmcomportindex);
            if (fCmdRet != 0)
            {
                string strLog = "Get Reader Information failed:" + GetReturnCodeDesc(fCmdRet);
                DC.ErrorLogFile(DC.ERRFILELOC, strLog);
                return false;
            }
            else
            {
                DC.setPara.powerDbm = powerdBm;
                DC.setPara.beep = BeepEn;
                DC.setPara.antenna = Ant;
                DC.setPara.sts = true;
                // power; 1= beep, 0 - close; 1248 - ant1,2,3,4
            }
            SetMode(1);

            timer_RealTime.Start();

            return true;
        }

        public bool setSettings()
        {
            bool sts = true;
            timer_RealTime.Stop();
            SetMode(0);
            byte powerDbm = DC.setPara.powerDbm > 30? (byte)30: DC.setPara.powerDbm;
            fCmdRet = RWDev.SetRfPower(ref fComAdr, powerDbm, frmcomportindex);
            if (fCmdRet != 0)
            {
                string strLog = "Set power failed: " + GetReturnCodeDesc(fCmdRet);
                DC.ErrorLogFile(DC.ERRFILELOC, strLog);
                sts = false;
            }
            fCmdRet = RWDev.SetBeepNotification(ref fComAdr, DC.setPara.beep, frmcomportindex);
            if (fCmdRet != 0)
            {
                string strLog = "Set beep failed: " + GetReturnCodeDesc(fCmdRet);
                DC.ErrorLogFile(DC.ERRFILELOC, strLog);
                sts = false;
            }
            fCmdRet = RWDev.SetAntennaMultiplexing(ref fComAdr, DC.setPara.antenna, frmcomportindex);
            if (fCmdRet != 0)
            {
                string strLog = "Antenna config failed: " + GetReturnCodeDesc(fCmdRet);
                DC.ErrorLogFile(DC.ERRFILELOC, strLog);
                sts = false;
            }
            SetMode(1);
            timer_RealTime.Start();
            return sts;
        }

        public int setPower(string power)
        {
            return 0; //throw new NotImplementedException();
        }

        public bool tagDetection()
        {
            bool sts = false;
            if (mainString != "")
            {
                string tempMainString = mainString;
                string[] temp = mainString.Split(';');
                var result = prevSerialnumber;
                prevSerialnumber = "";
                for (int i = 0; i < temp.Length-1; i++)
                {
                    string epc = temp[i].Split(',')[0];
                    if (result.Contains(epc + ";"))
                    {

                    }
                    else
                    {
                        DC.opString += temp[i] + ";";
                        result += epc + ";";
                    }
                    if (!prevSerialnumber.Contains(epc + ";"))
                        prevSerialnumber += epc + ";";
                }
                mainString = mainString.Remove(0, tempMainString.Length);
            }
            else
                prevSerialnumber = "";

            if (DC.opString != "")
                sts = true;
            return sts;
        }

        public string tagRead()
        {
            if (DC.opString != "")
            {
                string itemId = DC.opString.Split(';')[0];
                DC.opString = DC.opString.Remove(0, itemId.Length + 1);
                
                return itemId;
            }
            return "";
        }

        public string SetMode(byte mode)
        {

            fCmdRet = RWDev.SetReadMode(ref fComAdr, mode, frmcomportindex);

            if (fCmdRet != 0)
            {
                return( "Set read mode failed: " + GetReturnCodeDesc(fCmdRet));
            }
            else
            {
                return "Success";
            }
        }

        private string GetReturnCodeDesc(int cmdRet)
        {
            switch (cmdRet)
            {
                case 0x00:
                case 0x26:
                    return "success";
                case 0x01:
                    return "Return before Inventory finished";
                case 0x02:
                    return "the Inventory-scan-time overflow";
                case 0x03:
                    return "More Data";
                case 0x04:
                    return "Reader module MCU is Full";
                case 0x05:
                    return "Access Password Error";
                case 0x09:
                    return "Destroy Password Error";
                case 0x0a:
                    return "Destroy Password Error Cannot be Zero";
                case 0x0b:
                    return "Tag Not Support the command";
                case 0x0c:
                    return "Use the commmand,Access Password Cannot be Zero";
                case 0x0d:
                    return "Tag is protected,cannot set it again";
                case 0x0e:
                    return "Tag is unprotected,no need to reset it";
                case 0x10:
                    return "There is some locked bytes,write fail";
                case 0x11:
                    return "can not lock it";
                case 0x12:
                    return "is locked,cannot lock it again";
                case 0x13:
                    return "Parameter Save Fail,Can Use Before Power";
                case 0x14:
                    return "Cannot adjust";
                case 0x15:
                    return "Return before Inventory finished";
                case 0x16:
                    return "Inventory-Scan-Time overflow";
                case 0x17:
                    return "More Data";
                case 0x18:
                    return "Reader module MCU is full";
                case 0x19:
                    return "'Not Support Command Or AccessPassword Cannot be Zero";
                case 0x1A:
                    return "Tag custom function error";
                case 0xF8:
                    return "Check antenna error";
                case 0xF9:
                    return "Command execute error";
                case 0xFA:
                    return "Get Tag,Poor Communication,Inoperable";
                case 0xFB:
                    return "No Tag Operable";
                case 0xFC:
                    return "Tag Return ErrorCode";
                case 0xFD:
                    return "Command length wrong";
                case 0xFE:
                    return "Illegal command";
                case 0xFF:
                    return "Parameter Error";
                case 0x30:
                    return "Communication error";
                case 0x31:
                    return "CRC checksummat error";
                case 0x32:
                    return "Return data length error";
                case 0x33:
                    return "Communication busy";
                case 0x34:
                    return "Busy,command is being executed";
                case 0x35:
                    return "ComPort Opened";
                case 0x36:
                    return "ComPort Closed";
                case 0x37:
                    return "Invalid Handle";
                case 0x38:
                    return "Invalid Port";
                case 0xEE:
                    return "Return Command Error";
                default:
                    return "";
            }
        }

        private void timer_RealTime_Tick(object sender, EventArgs e)
        {
            if (strtInventoryScan) return;
            strtInventoryScan = true;
            GetRealtiemeData();
            strtInventoryScan = false;
        }

        private void GetRealtiemeData()
        {
            byte[] ScanModeData = new byte[40960];
            int nLen, NumLen;
            string temp1 = "";
            string EPCStr = "";
            string AntStr = "";
            int ValidDatalength;
            string temp;
            ValidDatalength = 0;
            int xtime = System.Environment.TickCount;
            try
            {
                fCmdRet = RWDev.ReadActiveModeData(ScanModeData, ref ValidDatalength, frmcomportindex);
                if (fCmdRet == 0)
                {

                    byte[] daw = new byte[ValidDatalength];
                    Array.Copy(ScanModeData, 0, daw, 0, ValidDatalength);
                    temp = ByteArrayToHexString(daw);
                    fInventory_EPC_List = fInventory_EPC_List + temp;
                    nLen = fInventory_EPC_List.Length;
                    while (fInventory_EPC_List.Length > 18)
                    {
                        string FlagStr = Convert.ToString(fComAdr, 16).PadLeft(2, '0') + "EE00";
                        int nindex = fInventory_EPC_List.IndexOf(FlagStr);
                        if (nindex > 1)
                            fInventory_EPC_List = fInventory_EPC_List.Substring(nindex - 2);
                        else
                        {
                            fInventory_EPC_List = fInventory_EPC_List.Substring(2);
                            continue;
                        }
                        NumLen = Convert.ToInt32(fInventory_EPC_List.Substring(0, 2), 16) * 2 + 2;//取第一个帧的长度
                        if (fInventory_EPC_List.Length < NumLen)
                        {
                            break;
                        }
                        temp1 = fInventory_EPC_List.Substring(0, NumLen);
                        fInventory_EPC_List = fInventory_EPC_List.Substring(NumLen);
                        if (!CheckCRC(temp1)) continue;
                        int antNo = Convert.ToInt32(temp1.Substring(8, 2));
                        AntStr = antNo == 1 ? "Ant1" : antNo == 2 ? "Ant2" : antNo == 4 ? "Ant3" : "Ant4";
                        EPCStr = temp1.Substring(12, temp1.Length - 18);
                        if (DC.connTypeSel.checkNeeded(EPCStr))
                        {
                            if (!mainString.Contains(EPCStr + ","))
                                mainString += EPCStr + "," + AntStr + "," + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + ";";
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                DC.ErrorLogFile(DC.ERRFILELOC, ex.ToString());
            }
            /*  else
              {
                  DC.ErrorLogFile(DC.ERRFILELOC, "CMDrET" + fCmdRet.ToString());
                 // closeRfidReader();
                 // DC.rfidfirsttime = false;
              }*/
        }

        public static string ByteArrayToHexString(byte[] data)
        {
            StringBuilder sb = new StringBuilder(data.Length * 3);
            foreach (byte b in data)
                sb.Append(Convert.ToString(b, 16).PadLeft(2, '0'));
            return sb.ToString().ToUpper();

        }

        private bool CheckCRC(string s)
        {
            int i, j;
            int current_crc_value;
            byte crcL, crcH;
            byte[] data = HexStringToByteArray(s);
            current_crc_value = 0xFFFF;
            for (i = 0; i <= (data.Length - 1); i++)
            {
                current_crc_value = current_crc_value ^ (data[i]);
                for (j = 0; j < 8; j++)
                {
                    if ((current_crc_value & 0x01) != 0)
                        current_crc_value = (current_crc_value >> 1) ^ 0x8408;
                    else
                        current_crc_value = (current_crc_value >> 1);
                }
            }
            crcL = Convert.ToByte(current_crc_value & 0xFF);
            crcH = Convert.ToByte((current_crc_value >> 8) & 0xFF);
            if (crcH == 0 && crcL == 0)
            {
                return true;
            }
            else
            {
                return false;
            }

        }

        public static byte[] HexStringToByteArray(string s)
        {
            s = s.Replace(" ", "");
            byte[] buffer = new byte[s.Length / 2];
            for (int i = 0; i < s.Length; i += 2)
                buffer[i / 2] = (byte)Convert.ToByte(s.Substring(i, 2), 16);
            return buffer;
        }

        public void alarmOn()
        {
        }

        public bool onlineReaderChk()
        {
            bool sts = false;
            try
            {
                SetMode(0);
                byte SaveLen = 0;
                fCmdRet = RWDev.GetSaveLen(ref fComAdr, ref SaveLen, frmcomportindex);
                if (fCmdRet == 0)
                {
                    sts = true;
                }
                else
                {
                    sts = false;
                }
                SetMode(1);
            }
            catch (Exception ex)
            {
                DC.ErrorLogFile(DC.ERRFILELOC, "c003onLINE" + ex.ToString());
                SetMode(1);
                return false;
            }
            return sts;
        }
    }
}
