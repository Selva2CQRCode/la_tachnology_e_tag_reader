﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RfidSdk
{
    public abstract class RfidReader
    {
        protected Transport transport;

        //receive data and handle it
        public abstract void OnRecvCompleted(byte[] messageData,int messageLen);

        public abstract void HandleRecvData();

        public void SetSerialParam(String serialPortName,int baudRate)
        {
            transport.SetSerialPortParam(serialPortName, baudRate);
        }

        public void SetEthernetParam(String localIP, UInt16 localPort, String remoteIP, UInt16 remotePort, TransportType type)
        {
            transport.SetIPParam(localIP, localPort, remoteIP, remotePort, type);
        }

        public Boolean RequestResource()
        {
            return transport.RequestResource();
        }

        public void ReleaseResource()
        {
            transport.ReleaseResource();
        }

        protected void FillCmdHeader(byte[] buff,byte frameCode)
        {
            int iIndex = 0;
            buff[iIndex++] = (byte)'R';
            buff[iIndex++] = (byte)'F';
            buff[iIndex++] = 0; //frame type
            buff[iIndex++] = 0;
            buff[iIndex++] = 0;
            buff[iIndex++] = frameCode;
        }

        protected byte CaculateCheckSum(byte[] recv_buff, int recv_len)
        {
            byte checksum = 0;
            for (int iIndex = 0; iIndex < recv_len; iIndex++)
            {
                checksum += recv_buff[iIndex];
            }
            checksum = (byte)(~checksum + 1);
            return checksum;
        }

        //Cmd for readers
        public void QueryDeviceInfo()
        {
            int pos = 6;
            byte temp = 0;
            byte[] cmdBuff = new byte[16];
            FillCmdHeader(cmdBuff, 0x40);
            cmdBuff[pos++] = 0;
            cmdBuff[pos++] = 0;
            temp = CaculateCheckSum(cmdBuff, pos);
            cmdBuff[pos++] = temp;
            transport.SendData(cmdBuff,0,pos);
            
        }

        public void StartInventory()
        {
            int pos = 6;
            byte temp = 0;
            byte[] cmdBuff = new byte[16];
            FillCmdHeader(cmdBuff, 0x21);
            cmdBuff[pos++] = 0;
            cmdBuff[pos++] = 0;
            temp = CaculateCheckSum(cmdBuff, pos);
            cmdBuff[pos++] = temp;
            transport.SendData(cmdBuff, 0, pos);
        }

        public void StopInventory()
        {
            int pos = 6;
            byte temp = 0;
            byte[] cmdBuff = new byte[16];
            FillCmdHeader(cmdBuff, 0x23);
            cmdBuff[pos++] = 0;
            cmdBuff[pos++] = 0;
            temp = CaculateCheckSum(cmdBuff, pos);
            cmdBuff[pos++] = temp;
            transport.SendData(cmdBuff, 0, pos);
        }

        public void QeryWorkingParam()
        {
            int pos = 6;
            byte temp = 0;
            byte[] cmdBuff = new byte[16];
            FillCmdHeader(cmdBuff, 0x42);
            cmdBuff[pos++] = 0;
            cmdBuff[pos++] = 0;
            temp = CaculateCheckSum(cmdBuff, pos);
            cmdBuff[pos++] = temp;
            transport.SendData(cmdBuff, 0, pos);
        }

        public void SetDefaultParam()
        {
            int pos = 6;
            byte temp = 0;
            byte[] cmdBuff = new byte[16];
            FillCmdHeader(cmdBuff, 0x12);
            cmdBuff[pos++] = 0;
            cmdBuff[pos++] = 0;
            temp = CaculateCheckSum(cmdBuff, pos);
            cmdBuff[pos++] = temp;
            transport.SendData(cmdBuff, 0, pos);
        }

        public void InventoryOnce()
        {
            int pos = 6;
            byte temp = 0;
            byte[] cmdBuff = new byte[16];
            FillCmdHeader(cmdBuff, 0x22);
            cmdBuff[pos++] = 0;
            cmdBuff[pos++] = 0;
            temp = CaculateCheckSum(cmdBuff, pos);
            cmdBuff[pos++] = temp;
            transport.SendData(cmdBuff, 0, pos);
        }

        public void ResetReader()
        {
            int pos = 6;
            byte temp = 0;
            byte[] cmdBuff = new byte[16];
            FillCmdHeader(cmdBuff, 0x10);
            cmdBuff[pos++] = 0;
            cmdBuff[pos++] = 0;
            temp = CaculateCheckSum(cmdBuff, pos);
            cmdBuff[pos++] = temp;
            transport.SendData(cmdBuff, 0, pos);
        }

        public void SetWorkingParam(RfidWorkParam workParam)
        {
            int pos = 6;
            byte temp = 0;
            byte[] cmdBuff = new byte[64];
            byte[] param_data = workParam.GetMessageDataFromParam();
            FillCmdHeader(cmdBuff, 0x41);
            cmdBuff[pos++] = (byte)(param_data.Length >> 8);
            cmdBuff[pos++] = (byte)(param_data.Length + 2);
            //add param ltv
            cmdBuff[pos++] = 0x23;
            cmdBuff[pos++] = (byte)param_data.Length;
            for (int index = 0; index < param_data.Length;index++)
            {
                cmdBuff[pos++] = param_data[index];
            }
            temp = CaculateCheckSum(cmdBuff, pos);
            cmdBuff[pos++] = temp;
            transport.SendData(cmdBuff, 0, pos);
        }

        public void QeryTransferParam()
        {
            int pos = 6;
            byte temp = 0;
            byte[] cmdBuff = new byte[16];
            FillCmdHeader(cmdBuff, 0x43);
            cmdBuff[pos++] = 0;
            cmdBuff[pos++] = 0;
            temp = CaculateCheckSum(cmdBuff, pos);
            cmdBuff[pos++] = temp;
            transport.SendData(cmdBuff, 0, pos);
        }

        public void SetTransferParam(RfidTransmissionParam transParam)
        {
            int pos = 6;
            byte temp = 0;
            byte[] cmdBuff = new byte[64];
            byte[] param_data = transParam.GetMessageDataFromParam();
            FillCmdHeader(cmdBuff, 0x44);
            cmdBuff[pos++] = (byte)(param_data.Length >> 8);
            cmdBuff[pos++] = (byte)(param_data.Length + 2);
            //add param ltv
            cmdBuff[pos++] = 0x24;
            cmdBuff[pos++] = (byte)param_data.Length;
            for (int index = 0; index < param_data.Length; index++)
            {
                cmdBuff[pos++] = param_data[index];
            }
            temp = CaculateCheckSum(cmdBuff, pos);
            cmdBuff[pos++] = temp;
            transport.SendData(cmdBuff, 0, pos);
        }

        public void QeryAdvanceParam()
        {
            int pos = 6;
            byte temp = 0;
            byte[] cmdBuff = new byte[16];
            FillCmdHeader(cmdBuff, 0x45);
            cmdBuff[pos++] = 0;
            cmdBuff[pos++] = 0;
            temp = CaculateCheckSum(cmdBuff, pos);
            cmdBuff[pos++] = temp;
            transport.SendData(cmdBuff, 0, pos);
        }

        public void SetAdvanceParam(RfidAdvanceParam advanceParam)
        {
            int pos = 6;
            byte temp = 0;
            byte[] cmdBuff = new byte[64];
            byte[] param_data = advanceParam.GetMessageDataFromParam();
            FillCmdHeader(cmdBuff, 0x46);
            cmdBuff[pos++] = (byte)(param_data.Length >> 8);
            cmdBuff[pos++] = (byte)(param_data.Length + 2);
            //add param ltv
            cmdBuff[pos++] = 0x25;
            cmdBuff[pos++] = (byte)param_data.Length;
            for (int index = 0; index < param_data.Length; index++)
            {
                cmdBuff[pos++] = param_data[index];
            }
            temp = CaculateCheckSum(cmdBuff, pos);
            cmdBuff[pos++] = temp;
            transport.SendData(cmdBuff, 0, pos);
        }
    }
}
