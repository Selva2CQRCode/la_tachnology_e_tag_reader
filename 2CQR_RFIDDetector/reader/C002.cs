﻿using RfidSdk;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _2CQR
{
    public partial class C002 : RfidSdk.RfidReaderRspNotify,InterReaderTypeSel
    {
        private static int tagReadCount = 0;
        private static int noOfTags = 0;
        private static string[] serialnumber;
        private static string ItemId = "";
        static RfidReader reader = null;
        private static string mainString = "";
        private static RfidWorkParam rfidSet = new RfidWorkParam();
        private static RfidTransmissionParam rfidTrans = new RfidTransmissionParam();
        private static int powerSetStatus = -1;
        private static int TransformSetStatus = -1;
        private static string localIPSta = "";
        private static string initIPAddress;
        private static string initPort;
        private static string prevSerialnumber = "";
        string Power = "30";
        private static string MainIpAddr = "";
        private static string MainPort = "";
        IntPtr handle1 = IntPtr.Zero;
        public bool getSyncTotalData(string data)
        {
            return true;
        }
        public string initRfidReader(string IPAddr, string port, IntPtr handle)
        {
            closeRfidReader();
            handle1 = handle;
           initIPAddress = IPAddr;
            initPort = port;
            String remoteIp = IPAddr;
            UInt16 remotePort = UInt16.Parse(port);
            String localIp;
            UInt16 localPort = 9000;
            RfidSdk.TransportType type = RfidSdk.TransportType.UDP;
            IPAddress remoteAddr;
            if (false == IPAddress.TryParse(remoteIp, out remoteAddr))
            {
                DC.ErrorLogFile(DC.ERRFILELOC, "E001:" + "The IP address is in error format.");
                return ("E001:" + "The IP address is in error format.");
            }
            localIp = GetLocalIP(remoteAddr);
            localIPSta = localIp;
            try
            {
                reader = new MSerialReader(this);
                reader = RfidReaderManager.Instance().CreateReaderInNet(localIp, localPort, remoteIp, remotePort, type, this);
                if (null != reader)
                {
                    MainIpAddr = remoteIp;
                    MainPort = port;
                    reader.StartInventory();
                    return "Success";
                }
                DC.ErrorLogFile(DC.ERRFILELOC, "E002: Cannot connect reader");

                return "E002: Cannot connect reader";
            }
            catch (Exception ex)
            {
                DC.ErrorLogFile(DC.ERRFILELOC, ex.ToString());
                return ("E003-" + ex.ToString());
            }
        }


        public bool tagDetection()
        {
            bool sts = false;
            if (mainString != "") 
            {
                string tempMainString = mainString;
                string[] temp = mainString.Split(';');
                var result = prevSerialnumber;
                prevSerialnumber = "";
                for (int i = 0; i < temp.Length; i++)
                {
                    string epc = temp[i].Split(',')[0];
                    if (result.Contains(epc + ";"))
                    {

                    }
                    else
                    {
                        DC.opString += temp[i] + ";";
                        result += epc + ";";
                    }
                    if (!prevSerialnumber.Contains(epc + ";"))
                        prevSerialnumber += epc + ";";
                }
                mainString = mainString.Remove(0, tempMainString.Length);
            }
            else
                prevSerialnumber = "";

            if (DC.opString != "")
                sts = true;

            return sts;
        }

        public string tagRead()
        {
            if (DC.opString != "")
            {
                string itemId = DC.opString.Split(';')[0];
                DC.opString = DC.opString.Remove(0, itemId.Length + 1);

                return itemId;
            }
            return "";
        }

        public int setPower(string power)
        {
            Power = power;
            powerSetStatus = -1;

            reader.QeryWorkingParam();
            while (powerSetStatus == -1)
            {
            }
            return powerSetStatus;
        }

        public void closeRfidReader()
        {
            try
            {
                if (reader != null)
                {
                    reader.StopInventory();
                    RfidReaderManager.Instance().ReleaseRfidReader(reader);
                    reader = null;
                }
            }
            catch
            {

            }
        }

        private static string GetLocalIP(IPAddress remoteAddress)
        {
            byte[] localIP;
            byte[] remoteIP = remoteAddress.GetAddressBytes();
            int selectIndex = 0;
            int compare = 0;
            int lastSameCount = 0;
            string IPAdrees = "";
            IPHostEntry IpEntry;
            try
            {
                string HostName = System.Net.Dns.GetHostName();
                IpEntry = Dns.GetHostEntry(HostName);
                for (int i = 0; i < IpEntry.AddressList.Length; i++)
                {
                    compare = 0;
                    if (IpEntry.AddressList[i].AddressFamily == remoteAddress.AddressFamily)
                    {
                        localIP = IpEntry.AddressList[i].GetAddressBytes();
                        for (int index = 0; index < remoteIP.Length; index++)
                        {
                            if (remoteIP[index] == localIP[index])
                            {
                                compare++;
                            }
                        }
                        if (compare > lastSameCount)
                        {
                            lastSameCount = compare;
                            selectIndex = i;
                            IPAdrees += IpEntry.AddressList[selectIndex].ToString() + ",";
                        }
                    }

                }
                return IpEntry.AddressList[selectIndex].ToString();
            }
            catch (Exception ex)
            {
                return ex.Message;
            }

        }

        public void OnRecvTagNotify(RfidReader reader1, TlvValueItem[] tlvItems, byte tlvCount)
        {
            //    MessageBox.Show("Entered tag notify");
            if (reader1 != reader || null == tlvItems || tlvItems.Length < tlvCount)
            {
                return;
            }
            for (int index = 0; index < tlvCount; index++)
            {
                if (tlvItems[index]._tlvType == (byte)RfidSdk.Tlv_Attr_Code.TLV_ATTR_CODE_EPC)
                {
                    string temp = ""; ;
                    for (int tagIndex = 0; tagIndex < tlvItems[index]._tlvLen; tagIndex++)
                    {
                        temp += tlvItems[index]._tlvValue[tagIndex].ToString("X2");
                    }
                    if (temp.Length == 12 || temp.Length == 24)
                    {
                        if (DC.connTypeSel.checkNeeded(temp))
                        {
                            if (!mainString.Contains(temp + ",,"))
                            {
                                mainString += temp + ",," + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + ",CardReader;";
                            }
                        }
                    }
                }
            }
        }

        public void OnRecvQueryWorkParamRsp(RfidReader reader1, byte result, RfidWorkParam workParam)
        {
            if (reader1 != reader)
            {
                return;
            }
            powerSetStatus = 1;
        }

        public void OnRecvSetWorkParamRsp(RfidReader reader, byte result)
        {
            RfidReaderManager.Instance().ReleaseRfidReader(reader);
            if (0 == result)
            {
                powerSetStatus = 0;
            }
            else
            {
                powerSetStatus = 1;
            }

        }

        #region notneccsary
        public void OnRecvResetRsp(RfidReader reader, byte result)
        {
        }

        public void OnRecvSetFactorySettingRsp(RfidReader reader, byte result)
        {
        }

        public void OnRecvStartInventoryRsp(RfidReader reader, byte result)
        {
            //MessageBox.Show("Starteed");
        }

        public void OnRecvStopInventoryRsp(RfidReader reader, byte result)
        {
        }

        public void OnRecvDeviceInfoRsp(RfidReader reader, byte[] firmwareVersion, byte deviceType)
        {
        }

        public void OnRecvSetTransmissionParamRsp(RfidReader reader, byte result)
        {
            if (0 == result)
            {
                TransformSetStatus = 0;
            }
            else
            {
                TransformSetStatus = 1;
            }
        }

        public void OnRecvQueryTransmissionParamRsp(RfidReader reader1, byte result, RfidTransmissionParam transmissiomParam)
        {
            if (reader1 != reader)
            {
                return;
            }
            if (result == 0)
            {
                rfidTrans = transmissiomParam;
                //   MessageBox.Show(rfidTrans.ucTransferMode.ToString());
                /*   for (int index = 0; index < 4; index++)
                   {
                       MessageBox.Show(rfidTrans.remote_ip_addr[index].ToString());
                   }*/
                rfidTrans.ucTransferMode = 3;
                // MessageBox.Show(localIPSta.ToString());
                String[] strRemoteIp = localIPSta.Split('.');
                for (int index = 0; index < 4; index++)
                {
                    rfidTrans.remote_ip_addr[index] = Convert.ToByte(strRemoteIp[index]);
                }
                reader.SetTransferParam(rfidTrans);
            }
        }

        public void OnRecvSetAdvanceParamRsp(RfidReader reader, byte result)
        {
        }

        public void OnRecvQueryAdvanceParamRsp(RfidReader reader, byte result, RfidAdvanceParam advanceParam)
        {
        }
        #endregion

        // To open a log file
        public static void ErrorLogFile(string errFileLoc, string errMsg)
        {
            try
            {
                StreamWriter ErrorLog;
                if (!File.Exists(errFileLoc))
                {
                    ErrorLog = new StreamWriter(errFileLoc);
                }
                else
                {
                    ErrorLog = File.AppendText(errFileLoc);
                }
                // Write the Err Msg in Log
                ErrorLog.WriteLine("( " + DateTime.Now.ToString() + " ) - " + errMsg);
                // Close the Log
                ErrorLog.Close();
            }
            catch
            {

            }
        }

        public bool getSettings()
        {
            return false;
        }

        public bool setSettings()
        {
            return false;
        }

        public void alarmOn()
        {
            
        }

        public bool onlineReaderChk()
        {
            try
            {
                if (initRfidReader(MainIpAddr, MainPort, handle1) == "Success")
                    return true;
                else
                {
                    DC.ErrorLogFile(DC.ERRFILELOC, "Reader online fail C002");
                    return false;
                }
            }
            catch (Exception ex)
            {
                DC.ErrorLogFile(DC.ERRFILELOC, "EC0021001:Online Sts Ex " + ex.ToString());
                return false;
            }
        }
    }
}