﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _2CQR
{
    public class _2C003 : InterReaderTypeSel
    {
        private static C003 reader1 = new C003();
        private static DC003 reader2 = new DC003();
        private static string readerIp1 = "";
        private static string readerIp2 = "";
        private static string mainOpString = "";

        public bool getSyncTotalData(string data)
        {
            return true;
        }
        public void alarmOn()
        {
            reader1.alarmOn();
            reader2.alarmOn();
        }

        public void closeRfidReader()
        {
            reader1.closeRfidReader();
            reader2.closeRfidReader();
        }

        public bool getSettings()
        {
            return false;
        }

        public string initRfidReader(string IPAddr, string port, IntPtr Handle)
        {
            try
            {
                string[] ip = DC.dAHolder[(int)DC.dAName.ReaderIP].Split('*');
                readerIp1 = ip[0].Split(':')[0];
                readerIp2 = ip[1].Split(':')[0];
                string sts = "Error";
                if ((sts = reader1.initRfidReader(readerIp1, "27011", Handle)) == "Success")
                {
                    return (reader2.initRfidReader(readerIp2, "27011", Handle));
                }
                else
                {
                    return sts;
                }
            }
            catch
            {
                System.Windows.Forms.MessageBox.Show("Enter two reader Ips");
            }
            return "NO";
        }

        public bool onlineReaderChk()
        {
            if (reader1.onlineReaderChk())
            {
                if (reader2.onlineReaderChk())
                    return true;
                else
                    closeRfidReader();
            }
            else
                closeRfidReader();
            return false;
        }

        public int setPower(string power)
        {
            return 0;
        }

        public bool setSettings()
        {
            return false;
        }

        public bool tagDetection()
        {
            bool sts,sts1 = false;
            if((sts = reader1.tagDetection()) == true)
            {
                string[] temp = DC.opString.Split(';');
                for (int i = 0; i < temp.Length-1; i++)
                {
                    mainOpString += temp[i] + "," + readerIp1 + ";";
                }
            }
            DC.opString = "";
            if ((sts1 = reader2.tagDetection()) == true)
            {
                string[] temp = DC.opString.Split(';');
                for (int i = 0; i < temp.Length-1; i++)
                {
                    mainOpString += temp[i] + "," + readerIp2 + ";";
                }
            }
            DC.opString = "";
            if (sts == true || sts1 == true)
                return true;
            return false;
        }

        public string tagRead()
        {
            if (mainOpString != "")
            {
                string itemId = mainOpString.Split(';')[0];
                mainOpString = mainOpString.Remove(0, itemId.Length + 1);
                return itemId;
            }
            return "";
        }
    }
}
