﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ReaderB;
using System.Threading;

namespace _2CQR
{
    public partial class C001 : InterReaderTypeSel
    {
        private static byte fComAdr = 0xff;
        private static int frmcomportindex;
        private int fCmdRet = 30;
        private static string mainString = "";
        private static string prevSerialnumber = "";
        private static bool strtInventoryScan = false;
        Thread MyThread;
       // private static System.Windows.Forms.Timer timer_RealTime = new System.Windows.Forms.Timer();
        public bool getSyncTotalData(string data)
        {
            return true;
        }
        public string initRfidReader(string IPAddr, string port, IntPtr Handle)
        {
            int openresult;
            openresult = 30;
            Int32 por = Convert.ToInt32(port);
            fComAdr = Convert.ToByte("FF", 16); // "ff"
            try
            {
                openresult = StaticClassReaderB.OpenNetPort(por, IPAddr, ref fComAdr, ref frmcomportindex);
                if (openresult == 0)
                {
                    //timer_RealTime.Interval = 40;
                    MyThread = new Thread(new ThreadStart(timer_RealTime_Tick));
                    MyThread.IsBackground = true;
                    MyThread.Start();
                  //  timer_RealTime.Tick += new System.EventHandler(timer_RealTime_Tick);
                  //  timer_RealTime.Start();
                    return "Success";
                }
                if ((openresult == 0x35) || (openresult == 0x30))
                {
                    StaticClassReaderB.CloseNetPort(frmcomportindex);
                    return ("E001");
                }
            }
            catch (Exception ex)
            {
                return ("E002" + ex.ToString());
            }
            return "E003";
        }

        //  private void timer_RealTime_Tick(object sender, EventArgs e)
        private void timer_RealTime_Tick()
        {
            while (true)
            {
                if (strtInventoryScan) return;
                strtInventoryScan = true;
                GetRealtiemeData();
                strtInventoryScan = false;
            }
        }

        public void GetRealtiemeData()
        { 
            try
            {
                int CardNum = 0;
                int Totallen = 0;
                int EPClen;
                int i = 0;
                string sEPC;
                byte[] EPC = new byte[5000];
                int j = 0;
                fCmdRet = StaticClassReaderB.Inventory_G2(ref fComAdr, 0, 0, 0, EPC, ref Totallen, ref CardNum, frmcomportindex);
                if ((fCmdRet == 1) | (fCmdRet == 2) | (fCmdRet == 3) | (fCmdRet == 4) | (fCmdRet == 0xFB))//代表已查找结束，
                {
                    byte[] epcByte = new byte[Totallen];

                    Array.Copy(EPC, epcByte, Totallen);
                    string tempSnoString = ByteArrayToHexString(epcByte);
                    for (int CardIndex = 0; CardIndex < CardNum; CardIndex++)
                    {
                        EPClen = epcByte[i];
                        sEPC = tempSnoString.Substring(i * 2 + 2, EPClen * 2);
                        i = i + EPClen + 1;
                        if (sEPC.Length == EPClen * 2)
                        {
                            if (DC.connTypeSel.checkNeeded(sEPC))
                            {
                                if (!mainString.Contains(sEPC + ","))
                                    mainString += sEPC + "," + "" + "," + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + ";";
                            }
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                DC.ErrorLogFile(DC.ERRFILELOC, "EC001001:" + ex.ToString());
            }
        }

        public bool tagDetection()
        {
            bool sts = false;
            if (mainString != "")
            {
                string tempMainString = mainString;
                string[] temp = mainString.Split(';');
                var result = prevSerialnumber;
                prevSerialnumber = "";
                for (int i = 0; i < temp.Length; i++)
                {
                    string epc = temp[i].Split(',')[0];
                    if (result.Contains(epc + ";"))
                    {

                    }
                    else
                    {
                        DC.opString += temp[i] + ";";
                        result += epc + ";";
                    }
                    if (!prevSerialnumber.Contains(epc + ";"))
                        prevSerialnumber += epc + ";";
                }
                mainString = mainString.Remove(0, tempMainString.Length);
            }
            else
                prevSerialnumber = "";

            if (DC.opString != "")
                sts = true;
            return sts;
        }

        public string tagRead()
        {
            if (DC.opString != "")
            {
                string itemId = DC.opString.Split(';')[0];
                DC.opString = DC.opString.Remove(0, itemId.Length + 1);

                return itemId;
            }
            return "";
        }
      /*  public string tagRead()
        {
            if (tagReadCount == noOfTags)
            {
                tagReadCount = 0;
                return "";
            }
            else
            {
                ItemId = serialnumber[tagReadCount++];
                return ItemId;
            }
        }*/
        private string ByteArrayToHexString(byte[] data)
        {
            StringBuilder sb = new StringBuilder(data.Length * 3);
            foreach (byte b in data)
                sb.Append(Convert.ToString(b, 16).PadLeft(2, '0'));
            return sb.ToString().ToUpper();

        }

        public int setPower(string power)
        {
            byte powerDbm = Convert.ToByte(power);
            fCmdRet = StaticClassReaderB.SetPowerDbm(ref fComAdr, powerDbm, frmcomportindex);
            return fCmdRet;
        }

        public void closeRfidReader()
        {
            MyThread.Abort();
            fCmdRet = StaticClassReaderB.CloseNetPort(frmcomportindex);
            if (fCmdRet != 0)
            {
                MessageBox.Show("Close RFID Failed" + fCmdRet.ToString());
            }
        }

        public bool getSettings()
        {
            bool sts = false;
            try
            {
                byte[] TrType = new byte[2];
                byte[] VersionInfo = new byte[2];
                byte ReaderType = 0;
                byte ScanTime = 0;
                byte dmaxfre = 0;
                byte dminfre = 0;
                byte powerdBm = 0;
                fCmdRet = StaticClassReaderB.GetReaderInformation(ref fComAdr, VersionInfo, ref ReaderType, TrType, ref dmaxfre, ref dminfre, ref powerdBm, ref ScanTime, frmcomportindex);
                if (fCmdRet == 0)
                {
                    DC.setPara.powerDbm = (byte)(powerdBm > 30?30:powerdBm);
                    DC.setPara.beep = 0;
                    DC.setPara.antenna = 0;
                    DC.setPara.sts = true;
                    sts = true;
                }
            }
            catch (Exception e)
            {
                DC.ErrorLogFile(DC.ERRFILELOC, e.ToString());
            }
            return sts;
        }

        public bool setSettings()
        {
            bool sts = false;
            try
            {
                byte powerDbm = Convert.ToByte(DC.setPara.powerDbm);
                fCmdRet = StaticClassReaderB.SetPowerDbm(ref fComAdr, powerDbm, frmcomportindex);
                if (fCmdRet == 0)
                    sts = true;
            }
            catch(Exception e)
            {
                DC.ErrorLogFile(DC.ERRFILELOC, e.ToString());
            }
            return sts;
        }

        public void alarmOn()
        {
          //  throw new NotImplementedException();
        }

        public bool onlineReaderChk()
        {
            bool sts = false;
            try
            {
                byte[] TrType = new byte[2];
                byte[] VersionInfo = new byte[2];
                byte ReaderType = 0;
                byte ScanTime = 0;
                byte dmaxfre = 0;
                byte dminfre = 0;
                byte powerdBm = 0;
                fCmdRet = StaticClassReaderB.GetReaderInformation(ref fComAdr, VersionInfo, ref ReaderType, TrType, ref dmaxfre, ref dminfre, ref powerdBm, ref ScanTime, frmcomportindex);
                if (fCmdRet == 0)
                {
                    sts = true;
                }
            }
            catch(Exception e)
            {
                DC.ErrorLogFile(DC.ERRFILELOC, e.ToString());
            }
            return sts;
        }
    }
}
