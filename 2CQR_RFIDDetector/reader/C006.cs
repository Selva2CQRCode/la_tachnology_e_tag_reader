﻿using System;
using System.Collections.Generic;
using System.Text;
using MyReaderAPI;
using MyReaderAPI.MyInterface;
using MyReaderAPI.Models;
using System.Linq;

namespace _2CQR
{
    //Chafon - 4 port reader
    public class C006 : InterReaderTypeSel, IAsynchronousMessage
    {
        private static string mainString = "";
        private static string prevSerialnumber = "";
        private static int[] count = new int[500];
        private static System.Windows.Forms.Timer alrmTimer;
        private static int alarmFirst = 0;

        public bool getSyncTotalData(string data)
        {
            return true;
        }
        public void closeRfidReader()
        {
            try
            {
                // stop alarm
                String rt = MyReaderAPI.MyReader.PARAM_SET.SetReaderGPOState(DC.dAHolder[(int)DC.dAName.ReaderIP], "1,0");
                // Disconnect the reader
                MyReaderAPI.MyReader.RFID_OPTION.StopReader(DC.dAHolder[(int)DC.dAName.ReaderIP]);
            }
            catch
            {

            }
        }

        public string initRfidReader(string IPAddr, string port, IntPtr Handle)
        {
            alrmTimer = new System.Windows.Forms.Timer();
            alrmTimer.Tick += AlrmTimer_Tick;
            alrmTimer.Interval = 1;
            bool isConnect = false;
            try
            {
                // Disconnect the reader
                MyReaderAPI.MyReader.RFID_OPTION.StopReader(DC.dAHolder[(int)DC.dAName.ReaderIP]);
            }
            catch
            {

            }
            try
            {
                if (isConnect = MyReader.CreateTcpConn(DC.dAHolder[(int)DC.dAName.ReaderIP], this))
                {
                    // Start the inventory
                    string tempr = MyReaderAPI.MyReader.RFID_OPTION.GetEPC(DC.dAHolder[(int)DC.dAName.ReaderIP], DC.dAHolder[(int)DC.dAName.C6Ant] + "|1");
                    if (tempr.StartsWith("0"))
                    {
                        // stop alarm
                        String rt = MyReaderAPI.MyReader.PARAM_SET.SetReaderGPOState(DC.dAHolder[(int)DC.dAName.ReaderIP], "1,0");
                        alrmTimer.Start();
                        return "Success";
                    }
                    else
                    {
                        DC.ErrorLogFile(DC.ERRFILELOC, "Reader Not Started");
                        return "E001: Reader Not Started";
                    }
                }
                DC.ErrorLogFile(DC.ERRFILELOC, "EC006001:");
                return ("E002:");
            }
            catch (Exception e)
            {
                DC.ErrorLogFile(DC.ERRFILELOC, "EC006002:" + e.ToString());
                return ("E003:");
            }
            /*  try
              {
                  listUsbDevicePath = MyReader.GetUsbHidDeviceList();
                  bool isConnect = false;
                  try
                  {
                      // Disconnect the reader
                      MyReaderAPI.MyReader.RFID_OPTION.StopReader(listUsbDevicePath[0]);
                  }
                  catch
                  {

                  }
                  if (isConnect = MyReader.CreateUsbConn(listUsbDevicePath[0], Handle, this))
                  {
                      // Start the inventory
                      string tempr = MyReaderAPI.MyReader.RFID_OPTION.GetEPC(listUsbDevicePath[0], DC.dAHolder[(int)DC.dAName.C6Ant] +"|1");
                      if (tempr.StartsWith("0"))
                      {

                          DC.ErrorLogFile(DC.ERRFILELOC, "connected");
                          alrmTimer.Start();
                          return "Success";
                      }
                      else
                      {
                          DC.ErrorLogFile(DC.ERRFILELOC, "Reader Not Started");
                          return "E001: Reader Not Started";
                      }
                  }
                  DC.ErrorLogFile(DC.ERRFILELOC, "EC006001:");
                  return ("E002:");
              }
              catch (Exception e)
              {
                  DC.ErrorLogFile(DC.ERRFILELOC, "EC006002:" + e.ToString());
                  return ("E003:");
              }*/
        }

        private void AlrmTimer_Tick(object sender, EventArgs e)
        {
            if (alarmFirst == 1)
            {
                alrmTimer.Stop();
                alarmFirst = 2;
                String rt = MyReaderAPI.MyReader.PARAM_SET.SetReaderGPOState(DC.dAHolder[(int)DC.dAName.ReaderIP], "1,1");
                alrmTimer.Interval = 5000;
                alrmTimer.Start();
            }
            else if (alarmFirst == 2)
            {
                alrmTimer.Stop();
                alarmFirst = 0;
                String rt = MyReaderAPI.MyReader.PARAM_SET.SetReaderGPOState(DC.dAHolder[(int)DC.dAName.ReaderIP], "1,0");
                alrmTimer.Interval = 1;
                alrmTimer.Start();
            }
        }

        public bool getSettings()
        {
            try
            {
                DC.setPara.antenna = byte.Parse(DC.dAHolder[(int)DC.dAName.C6Ant]);
                DC.setPara.beep = 0;
                String rtStr = MyReaderAPI.MyReader.RFID_OPTION.GetANTPowerParam(DC.dAHolder[(int)DC.dAName.ReaderIP]);
                String[] varStr = rtStr.Split('&');
                DC.setPara.powerDbm = byte.Parse(varStr[0].Split(',')[1]);
                DC.setPara.sts = true;
            }
            catch
            {
                string strLog = "Get power settings Failed";
                DC.ErrorLogFile(DC.ERRFILELOC, strLog);
                return false;
            }

            return true;
        }

        public bool getReaderOnlineStsChk()
        {
            try
            {
                DC.setPara.antenna = byte.Parse(DC.dAHolder[(int)DC.dAName.C6Ant]);
                DC.setPara.beep = 0;
                String rtStr = MyReaderAPI.MyReader.RFID_OPTION.GetANTPowerParam(DC.dAHolder[(int)DC.dAName.ReaderIP]);
                String[] varStr = rtStr.Split('&');
                DC.setPara.powerDbm = byte.Parse(varStr[0].Split(',')[1]);
                DC.setPara.sts = true;
            }
            catch
            {
                string strLog = "Get power settings Failed";
                DC.ErrorLogFile(DC.ERRFILELOC, strLog);
                return false;
            }

            return true;
        }

        public bool setSettings()
        {
            bool sts = true;
            try
            {
                // Set antenna power
                int temp = int.Parse(DC.dAHolder[(int)DC.dAName.C6Ant]);
                string power = "";
                if ((temp & 0x01) == 1)
                    power += "1," + DC.setPara.powerDbm + "&";
                if ((temp & 0x02) == 2)
                    power += "2," + DC.setPara.powerDbm + "&";
                if ((temp & 0x04) == 4)
                    power += "3," + DC.setPara.powerDbm + "&";
                if ((temp & 0x08) == 8)
                    power += "4," + DC.setPara.powerDbm + "&";
                power = power.Remove(power.Length - 1, 1);
                String rtStr = MyReaderAPI.MyReader.RFID_OPTION.SetANTPowerParam(DC.dAHolder[(int)DC.dAName.ReaderIP], power);
            }
            catch (Exception e)
            {
                string strLog = "Set power failed: ";
                DC.ErrorLogFile(DC.ERRFILELOC, strLog);
                sts = false;
            }
            return sts;
        }

        public int setPower(string power)
        {
            return 0; //throw new NotImplementedException();
        }

        public bool tagDetection()
        {
            bool sts = false;
            if (mainString != "")
            {
                string tempMainString = mainString;
                string[] temp = mainString.Split(';');
                int j = 0;
                var result = prevSerialnumber;
                prevSerialnumber = "";
                for (int i = 0; i < temp.Length; i++)
                {
                    string epc = temp[i].Split(',')[0];
                    if (result.Contains(epc + ";"))
                    {

                    }
                    else
                    {
                        DC.opString += temp[i] + ";";
                        result += epc + ";";
                    }
                    if (!(prevSerialnumber.Contains(epc + ";")))
                        prevSerialnumber += epc + ";";
                }
                mainString = mainString.Remove(0, tempMainString.Length);
            }
            else
                prevSerialnumber = "";

            if (DC.opString != "")
                sts = true;
            return sts;
        }

        public string tagRead()
        {
            if (DC.opString != "")
            {
                string itemId = DC.opString.Split(';')[0];
                DC.opString = DC.opString.Remove(0, itemId.Length + 1);
                return itemId;
            }
            return "";
        }

        public string SetMode(byte mode)
        {

            /* fCmdRet = RWDev.SetReadMode(ref fComAdr, mode, frmcomportindex);

             if (fCmdRet != 0)
             {
                 return ("Set read mode failed: " + GetReturnCodeDesc(fCmdRet));
             }
             else
             {
                 return "Success";
             }*/
            return "";
        }

        public void WriteDebugMsg(string msg)
        {
        }

        public void WriteLog(string msg)
        {
        }

        public void PortConnecting(string connID)
        {
        }

        public void PortClosing(string connID)
        {
        }

        public void OutPutTags(Tag_Model tag)
        {
            // Output Tag Data
            if (!mainString.Contains(tag.EPC + ","))
            {
                mainString += tag.EPC + "," + "ANT" + tag.ANT_NUM + "," + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + ";";
            }
            try
            {
                string temp = tag.EPC.Substring(0, 2);
                if (temp == "31" || temp == "32" || temp == "33" || temp == "34" || temp == "35" || temp == "36" || temp == "37" || temp == "38" || temp == "39" || temp =="53")
                {
                    alarmFirst = alarmFirst == 0 ? 1 : alarmFirst;
                    string tagType = DC.dataModelTypeSel.getItemId(tag.EPC);
                    DC.ErrorLogFile(DC.UNAUTHLOC, tagType);
                }
            }
            catch
            {

            }
         //   if (!(tag.EPC.Contains("2D2D") || tag.EPC.Contains("2d2d") || tag.EPC.Contains("3F3F") || tag.EPC.Contains("3f3f")))
           
        }

        public void OutPutTagsOver()
        {
        }

        public void GPIControlMsg(int gpiIndex, int gpiState, int startOrStop)
        {
        }

        public void alarmOn()
        {
            /*if (alarmFirst == false)
            {
                alarmFirst = true;
                String rt = MyReaderAPI.MyReader.PARAM_SET.SetReaderGPOState(listUsbDevicePath[0], "1,0");
            }*/
        }

        public bool onlineReaderChk()
        {
            return true;
        }
    }
}
