﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ReaderB;
using System.Threading;

namespace _2CQR
{
    public partial class DemoReader : InterReaderTypeSel
    {
        private static int attempts = 0;
        public bool getSyncTotalData(string data)
        {
            return true;
        }
        public string initRfidReader(string IPAddr, string port, IntPtr Handle)
        {
            return "Success";
        }

        public bool tagDetection()
        {
            bool sts = false;
            if (attempts == 20)
            {
                DC.opString += "E012300001234,ANT1" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + ";";
                attempts = 0;
                return true;
            }
            else
                attempts++;
            return sts;
        }

        public string tagRead()
        {
            if (DC.opString != "")
            {
                string itemId = DC.opString.Split(';')[0];
                DC.opString = DC.opString.Remove(0, itemId.Length + 1);
                return itemId;
            }
            return "";
        }
        /*  public string tagRead()
          {
              if (tagReadCount == noOfTags)
              {
                  tagReadCount = 0;
                  return "";
              }
              else
              {
                  ItemId = serialnumber[tagReadCount++];
                  return ItemId;
              }
          }*/
      

        public int setPower(string power)
        {
            return 0;
        }

        public void closeRfidReader()
        {
           
        }

        public bool getSettings()
        {
            return false;
        }

        public bool setSettings()
        {
            return false;
        }

        public void alarmOn()
        {
            //  throw new NotImplementedException();
        }

        public bool onlineReaderChk()
        {
            return true;
        }
    }
}
