﻿using System;
using System.Runtime.InteropServices;

namespace _2CQR
{
    public class RData
    {
        [DllImport("plcommpro.dll")]
        public static extern IntPtr Connect(string Parameters);

        [DllImport("plcommpro.dll")]
        public static extern int PullLastError();

        [DllImport("plcommpro.dll")]
        public static extern int Disconnect();

        [DllImport("plcommpro.dll")]
        public static extern int GetDeviceParam();

        [DllImport("plcommpro.dll")]
        public static extern int GetDeviceParam(IntPtr h, ref byte buffer, int buffersize, string itemvalues);

        [DllImport("plcommpro.dll")]
        public static extern int GetDeviceData(IntPtr h, ref byte buffer, int buffersize, string tablename, string filename, string filter, string options);

        [DllImport("plcommpro.dll")]
        public static extern int GetRTLog(IntPtr h, ref byte buffer, int buffersize);

        [DllImport("plcommpro.dll")]
        public static extern int ControlDevice(IntPtr h, int operationid, int param1, int param2, int param3, int param4, string options);

        [DllImport("plcommpro.dll")]
        public static extern int SetDeviceData(IntPtr h, string tablename, string data, string options);

        [DllImport("plcommpro.dll")]
        public static extern int DeleteDeviceData(IntPtr h, string tablename, string data, string options);
    }
}
