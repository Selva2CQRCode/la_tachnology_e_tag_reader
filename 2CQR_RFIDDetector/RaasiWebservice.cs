﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Renci.SshNet;
using System.Net;
using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace _2CQR
{
    public class RasiWebService : InterConnectivity
    {
        private static int sno = 0;
        public string getRFIDTagData(string content)
        {
            return "";
        }
        public bool updateSync(string content)
        {
            return true;
        }

        public bool postSyncTotalData()
        {
            return true;
        }
        public void ClosingComm()
        {
        }

        public bool OnlineStsCheck()
        {
            return true;
        }

        public bool ServerConnection()
        {
            return true;
        }

        public string getUpdateContent(string Item)
        {
            sno++;
            string[] tempId = Item.Split(',');
            string itemIdFormat = DC.dataModelTypeSel.getItemId(tempId[0]);
            return (sno.ToString() + ";" + itemIdFormat + ";" + tempId[1] + ";" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
        }

        public bool update(string content)
        {
            try
            {
                DC.ErrorLogFile(DC.ERRFILELOC, "Entered Request");
                string[] tempContent = content.Split(',');

                string sttoken;
                var httpWebRequestQR = (HttpWebRequest)WebRequest.Create(@"http://" + DC.dAHolder[(int)DC.dAName.SIP] + "/authenticate");
                httpWebRequestQR.ContentType = "application/json";
                httpWebRequestQR.Method = "POST";
                using (var streamWriter = new StreamWriter(httpWebRequestQR.GetRequestStream()))
                {
                    string json = "{\"username\":\"" + DC.dAHolder[(int)DC.dAName.SUN] + "\"," +
                                  "\"password\":\"" + DC.dAHolder[(int)DC.dAName.Pass] + "\"}";

                    streamWriter.Write(json);
                }
                DC.ErrorLogFile(DC.ERRFILELOC, "Sending Request");

                var httpResponseQR = (HttpWebResponse)httpWebRequestQR.GetResponse();
                DC.ErrorLogFile(DC.ERRFILELOC, "Sending Request");

                if (httpResponseQR.StatusCode.ToString() == "OK")
                {
                    DC.ErrorLogFile(DC.ERRFILELOC , "StatusCodeToken:OK");
                    using (var streamReader = new StreamReader(httpResponseQR.GetResponseStream()))
                    {
                        var resultQR = streamReader.ReadToEnd();
                        JToken token = JObject.Parse(resultQR);
                        sttoken = (string)token.SelectToken("token");
                    }
                    var httpWebRequestQR1 = (HttpWebRequest)WebRequest.Create(@"http://" + DC.dAHolder[(int)DC.dAName.SIP] + "/antenna/feed");
                    httpWebRequestQR1.ContentType = "application/json";
                    httpWebRequestQR1.Headers.Add("Authorization", "Bearer " + sttoken);
                    httpWebRequestQR1.Method = "POST";
                    using (var streamWriter = new StreamWriter(httpWebRequestQR1.GetRequestStream()))
                    {
                        string json = "";
                        if (tempContent.Length == 3)
                            json = "{\"items\":[{\"tagId\":\"" + DC.dataModelTypeSel.getItemId(tempContent[0]) + "\",\"scanDateTime\":\"" + tempContent[2] + "\",\"readerIp\": \"" + DC.dAHolder[(int)DC.dAName.ReaderIP].Split(':')[0] + "\"}]}";
                        else
                            json = "{\"items\":[{\"tagId\":\"" + DC.dataModelTypeSel.getItemId(tempContent[0]) + "\",\"scanDateTime\":\"" + tempContent[2] + "\",\"readerIp\": \"" + tempContent[3] + "\"}]}";
                        streamWriter.Write(json);
                    }
                    var httpResponseQR1 = (HttpWebResponse)httpWebRequestQR1.GetResponse();
                    if (httpResponseQR1.StatusCode.ToString() == "OK")
                    {
                        DC.ErrorLogFile(DC.ERRFILELOC, "StatusCodePush:OK");
                        return true;
                    }
                    else
                    {
                        DC.ErrorLogFile(DC.ERRFILELOC, "PushDataFail" + httpResponseQR.StatusCode.ToString());
                        return false;
                    }
                }
                else
                {
                    DC.ErrorLogFile(DC.ERRFILELOC, "GetTokenFail" + httpResponseQR.StatusCode.ToString());
                    return false;
                }
            }
            catch (Exception ex)
            {
                DC.ErrorLogFile(DC.ERRFILELOC, "ServerProb" + ex.ToString());
                return false;
            }
        }

        public bool checkNeeded(string epc)
        {
            return true;
        }

        public bool Report(string query)
        {
            return true;
        }

        public string importData(string filename)
        {
            throw new NotImplementedException();
        }
    }
}
