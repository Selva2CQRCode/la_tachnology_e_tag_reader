﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace _2CQR
{
    public class Connectivity
    {
        // Timer to trigger server connection on loading
        public System.Windows.Forms.Timer serverTmr = new System.Windows.Forms.Timer();
        public System.Windows.Forms.Timer readerTmr = new System.Windows.Forms.Timer();

        public void initTimers()
        {
            serverTmr.Interval = 2;
            serverTmr.Tick += new EventHandler(serverTmrTick);
            serverTmr.Start();
            try { readerTmr.Interval = int.Parse(DC.dAHolder[(int)DC.dAName.ReaderOnlineTime]) * 1000; }
            catch(Exception e) { DC.ErrorLogFile(DC.ERRFILELOC, "ECN001001:" + e.ToString()); readerTmr.Interval = 1800000; }
            readerTmr.Tick += new EventHandler(readerTmrTick);
            readerTmr.Start();
        }

        public void stopTimer()
        {
            serverTmr.Stop();

        }

        // On triggering Server Timer Event
        public void serverTmrTick(object sender, EventArgs e)
        {
            serverTmr.Stop();
            if ((DC.connInitialized == false) && (DC.connTypeSel.ServerConnection() == false))
            {
                DC.connInitialized = false;
                serverTmr.Interval = Int32.Parse(DC.dAHolder[(int)DC.dAName.ServerInitialTime]) * 1000;
            }
            // else
            else if ((DC.connInitialized == true) && (DC.connTypeSel.OnlineStsCheck() == false))
            {
                serverTmr.Interval = 1;
                serverTmr.Interval = Int32.Parse(DC.dAHolder[(int)DC.dAName.ServerInitialTime]) * 1000;
                DC.connInitialized = false;
            }
            else
            {
                DC.connInitialized = true;
                serverTmr.Interval = Int32.Parse(DC.dAHolder[(int)DC.dAName.ServerInitialTime]) * 60000;
            }
            serverTmr.Start();
            DC.formTriggerTmr.Start();
        }

        public void readerTmrTick(object sender, EventArgs e)
        {
            readerTmr.Stop();
            if (DC.rfidfirsttime == false)
            {
                DC.ErrorLogFile(DC.ERRFILELOC, " reader health failed");
                DC.formTriggerTmr.Start();
            }
            else if (DC.readerTypeSel.onlineReaderChk() == false)
            {
                DC.rfidfirsttime = false;
            }
            readerTmr.Start();
        }
    }
}
