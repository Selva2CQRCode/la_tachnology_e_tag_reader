﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;

namespace _2CQR
{
    // For storing the Form values
    public static class AppSetting
    {
        // To get the key value stored
        public static string GetAppSetting(string key, int sNo)
        {
            //Download the AppSettings
            Configuration config = ConfigurationManager.
                                    OpenExeConfiguration(
                                    System.Reflection.Assembly.
                                    GetExecutingAssembly().Location);
            //Check whether key exists
            if (config.AppSettings.Settings[key] != null)
            {
                //Return the value associated to the key
                return config.AppSettings.Settings[key].Value;
            }
            else
            {
                // If key does not exist, return the default value
                return (DC.dAValue[sNo]);
            }
        }

        // To set the key value if changed
        public static void SetAppSetting(string key, string value)
        {
            //Download the App settings
            Configuration config = ConfigurationManager.
                                    OpenExeConfiguration(
                                    System.Reflection.Assembly.
                                    GetExecutingAssembly().Location);
            //Check whether key exists
            if (config.AppSettings.Settings[key] != null)
            {
                //Deleting the Keys to override it
                config.AppSettings.Settings.Remove(key);
            }
            //Creating a new KeyValue pair
            config.AppSettings.Settings.Add(key, value);
            //Save the updated AppSettings
            config.Save(ConfigurationSaveMode.Modified);
        }



    }
}
