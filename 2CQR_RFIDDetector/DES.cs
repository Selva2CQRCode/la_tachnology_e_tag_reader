﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Windows.Forms;

namespace _2CQR
{
    public class DES
    {
        public string DesEncryption(string original, byte[] key, byte[] iv)
        {
            string encrypted = null;
            try
            {
                // Create a new instance of the TripleDESCryptoServiceProvider 
                // class.  This generates a new key and initialization  
                // vector (IV). 
                using (TripleDESCryptoServiceProvider myTripleDES = new TripleDESCryptoServiceProvider())
                {
                    // Encrypt the string to an array of bytes. 
                    encrypted = EncryptStringToBytes(original, key, iv);
                }

            }
            catch (Exception ex)
            {
                DC.ErrorLogFile(DC.ERRFILELOC, "EDcDe001: " + ex.ToString());
            }
            return encrypted;
        }

        static string EncryptStringToBytes(string plainText, byte[] Key, byte[] IV)
        {
            try
            {
                // Check arguments. 
                // if (plainText == null || plainText.Length <= 0)
                //  throw new ArgumentNullException("plainText");
                if (Key == null || Key.Length <= 0)
                    throw new ArgumentNullException("Key");
                if (IV == null || IV.Length <= 0)
                    throw new ArgumentNullException("Key");
                string encrypted;
                byte[] encrypted1;
                // Create an TripleDESCryptoServiceProvider object 
                // with the specified key and IV. 
                using (TripleDESCryptoServiceProvider tdsAlg = new TripleDESCryptoServiceProvider())
                {
                    tdsAlg.Key = Key;
                    tdsAlg.IV = IV;

                    // Create a decrytor to perform the stream transform.
                    ICryptoTransform encryptor = tdsAlg.CreateEncryptor(tdsAlg.Key, tdsAlg.IV);

                    // Create the streams used for encryption. 
                    using (MemoryStream msEncrypt = new MemoryStream())
                    {
                        using (CryptoStream csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
                        {
                            using (StreamWriter swEncrypt = new StreamWriter(csEncrypt))
                            {
                                //Write all data to the stream.
                                swEncrypt.Write(plainText);
                            }
                            encrypted1 = msEncrypt.ToArray();
                            encrypted = Convert.ToBase64String(encrypted1);
                        }
                    }
                }


                // Return the encrypted bytes from the memory stream. 
                return encrypted;
            }
            catch (Exception ex)
            {
                DC.ErrorLogFile(DC.ERRFILELOC, "EDcDe002: " + ex.ToString());
                return null;
            }

        }

        public string DecryptStringFromBytes(string cipherText, byte[] Key, byte[] IV)
        {
            string plaintext = null;
            byte[] cipherText1 = Convert.FromBase64String(cipherText);

            // Check arguments. 
            //    if (cipherText == null || cipherText.Length <= 0)
            //     throw new ArgumentNullException("cipherText");
            if (Key == null || Key.Length <= 0)
                throw new ArgumentNullException("Key");
            if (IV == null || IV.Length <= 0)
                throw new ArgumentNullException("Key");

            // Declare the string used to hold 
            // the decrypted text. 


            // Create an TripleDESCryptoServiceProvider object 
            // with the specified key and IV. 
            using (TripleDESCryptoServiceProvider tdsAlg = new TripleDESCryptoServiceProvider())
            {
                tdsAlg.Key = Key;
                tdsAlg.IV = IV;

                // Create a decrytor to perform the stream transform.
                ICryptoTransform decryptor = tdsAlg.CreateDecryptor(tdsAlg.Key, tdsAlg.IV);

                // Create the streams used for decryption. 
                using (MemoryStream msDecrypt = new MemoryStream(cipherText1))
                {
                    using (CryptoStream csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
                    {
                        using (StreamReader srDecrypt = new StreamReader(csDecrypt))
                        {

                            // Read the decrypted bytes from the decrypting stream 
                            // and place them in a string.
                            plaintext = srDecrypt.ReadToEnd();
                        }
                    }
                }

            }
            return plaintext;
        }

    }
}
