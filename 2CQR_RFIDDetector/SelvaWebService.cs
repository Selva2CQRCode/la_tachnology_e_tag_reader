﻿namespace _2CQR
{
    using System;
    using System.IO;
    using System.Net;
    using System.Text;

    public class SelvaWebService : InterConnectivity
    {
        public bool checkNeeded(string epc)
        {
            return true;
        }

        public void ClosingComm()
        {
        }

        public string getRFIDTagData(string content)
        {
            return "";
        }

        public string importData(string filename)
        {
            throw new NotImplementedException();
        }

        public bool OnlineStsCheck()
        {
            return true;
        }

        public bool postSyncTotalData()
        {
            return true;
        }

        public bool Report(string query)
        {
            return true;
        }

        public bool ServerConnection()
        {
            return true;
        }

        public bool update(string content)
        {
            try
            {
                DC.ErrorLogFile(DC.ERRFILELOC, "Entered Request");
                char[] separator = new char[] { ',' };
                string[] strArray = content.Split(separator);
                ServicePointManager.Expect100Continue = true;
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12
                    | SecurityProtocolType.Ssl3
                    | SecurityProtocolType.Tls
                    | SecurityProtocolType.Tls11;
                //ServicePointManager.SecurityProtocol = (SecurityProtocolType)3072;
                //ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3;
                //System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
                //ServicePointManager.ServerCertificateValidationCallback = (snder, cert, chain, error) => true;
                var request = (HttpWebRequest) WebRequest.Create(@"https://" + DC.dAHolder[1] + "/latech/api/request/visitor_tracking");
                request.ContentType = "application/json";
                request.Method = "POST";
                request.Proxy = null;
                using (StreamWriter writer = new StreamWriter(request.GetRequestStream()))
                {
                    string str = "";
                    if (strArray.Length == 3)
                    {
                        string[] textArray1 = new string[7];
                        textArray1[0] = "{\"data\":[{\"rfid_number\":\"";
                        textArray1[1] = DC.dataModelTypeSel.getItemId(strArray[0]);
                       // textArray1[1] = "PL001";
                        textArray1[2] = "\",\"datetime\":\"";
                        textArray1[3] = strArray[2];
                        textArray1[4] = "\",\"ipaddress\": \"";
                        char[] chArray2 = new char[] { ':' };
                        textArray1[5] = DC.dAHolder[10].Split(chArray2)[0];
                        textArray1[6] = "\"}]}";
                        str = string.Concat(textArray1);
                    }
                    else
                    {
                        string[] textArray2 = new string[] { "{\"data\":[{\"rfid_number\":\"", DC.dataModelTypeSel.getItemId(strArray[0]), "\",\"datetime\":\"", strArray[2], "\",\"ipaddress\": \"", strArray[3], "\"}]}" };
                        str = string.Concat(textArray2);
                    }
                    writer.Write(str);
                }
                var response = (HttpWebResponse) request.GetResponse();
                if (response.StatusCode.ToString() == "OK")
                {
                    response.Dispose();
                    DC.ErrorLogFile(DC.ERRFILELOC, "StatusCodePush:OK");
                    return true;
                }
                response.Dispose();
                DC.ErrorLogFile(DC.ERRFILELOC, "PushDataFail" + response.StatusCode.ToString());
                return false;
            }
            catch (Exception exception)
            {
                DC.ErrorLogFile(DC.ERRFILELOC, "ServerProb" + exception.ToString());
                return false;
            }
        }
        private string HextoAscii(string hexString)
        {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < hexString.Length; i += 2)
            {
                string hs = hexString.Substring(i, 2);
                sb.Append(Convert.ToChar(Convert.ToUInt32(hs, 16)));
            }
            // MessageBox.Show(sb.ToString());
            return sb.ToString();

        }
        public bool updateSync(string content)
        {
            return true;
        }
    }
}

