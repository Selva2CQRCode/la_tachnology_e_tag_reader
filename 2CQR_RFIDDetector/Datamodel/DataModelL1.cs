﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace _2CQR
{
    public class DataModelL1
    {
        // To indicate if Unprogrammed
        public void BlankIndicator(byte[] tempData)
        {
            int cnt = 0;
            DC.blankIndicator = 0;
            for (cnt = 0; cnt < DC.DataModelCnt; cnt++)
            {
                if (tempData[cnt] != '\0')
                {
                    DC.blankIndicator = 1;
                    break;
                }
            }
        }

        public void BlankIndicator(char[] tempData)
        {
            int cnt = 0;
            DC.blankIndicator = 0;
            for (cnt = 0; cnt < DC.DataModelCnt; cnt++)
            {
                if (tempData[cnt] != '\0')
                {
                    DC.blankIndicator = 1;
                    break;
                }
            }
        }
    }
}
