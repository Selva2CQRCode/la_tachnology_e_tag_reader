﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _2CQR
{
    public class ASCII : InterDataModel
    {
        public string getItemId(string itemId)
        {
            string item = "";
            string temp1 = "";
            for (int i = 0; i < itemId.Length; i += 2)
            {
                try
                {
                    temp1 = itemId.Substring(i, 2);
                    if (temp1 != "00")
                        item += (char)Convert.ToInt32(temp1, 16);
                    temp1 = "";
                }
                catch
                {

                }
            }
            return item;
        }
    }
}
