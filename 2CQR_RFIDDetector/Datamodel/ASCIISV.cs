﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _2CQR
{
    public class ASCIISV : InterDataModel
    {
        public string getItemId(string itemId)
        {
            string item = "";
            string temp1 = "";
            DC.ErrorLogFile(DC.ERRFILELOC, "itemId:" + itemId + ":");
            for (int i = 0; i < itemId.Length; i += 2)
            {
                try
                {
                    temp1 = itemId.Substring(i, 2);

                    int temp2 = Convert.ToInt32(temp1, 16);
                    
                    if ((temp2 == 0) || (temp2 >= 33 && temp2 <= 126))
                    {
                        if (temp1 != "00")
                            item += (char)Convert.ToInt32(temp1, 16);
                    }
                    else
                    {
                        return null;
                    }
                    temp1 = "";
                }
                catch
                {

                }
            }
            DC.ErrorLogFile(DC.ERRFILELOC, "item:" + item + ":");
            return item;
        }
    }
}
