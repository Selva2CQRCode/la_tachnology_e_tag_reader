﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace _2CQR
{
    public class DESLoadStore
    {
        // Des decryption and storing in arrays
        public bool LoadConfiguration()
        {
            // Des Class calling for decrypting the value from the config file
            DES des = new DES();
            // For iterating the loop
            int cnt1 = 0;
            // To Store the encrypted value
            string[] tempdAHolder = new string[DC.dAValue.Length];
            // Get the default/ changed value of the form settings
            for (cnt1 = 0; cnt1 < DC.dAValue.Length; cnt1++)
            {
                // Get the encrypted value
                tempdAHolder[cnt1] = AppSetting.GetAppSetting(DC.dAKeys[cnt1], cnt1);
                try
                {
                    // Decrypt and store in the Array
                    DC.dAHolder[cnt1] = des.DecryptStringFromBytes(tempdAHolder[cnt1], UTF8Encoding.UTF8.GetBytes("BuMdFerf34poKVM1".ToCharArray()), UTF8Encoding.UTF8.GetBytes("78RTrtRT".ToCharArray()));
                }
                catch (Exception e)
                {
                    DC.ErrorLogFile(DC.ERRFILELOC, "EDcDe2001:" + e.ToString());
                    return false;
                }

            }
            return true;
        }
        public bool StoreConfiguration()
        {
            bool sts = true;
            DES des = new DES();
            string[] tempdAHolder = new string[DC.dAValue.Length];
            // For Iterating the loop
            int cnt1 = 0;
            // To save the settings made into Storage
            for (cnt1 = 0; cnt1 < DC.dAValue.Length; cnt1++)
            {
                try
                {
                    tempdAHolder[cnt1] = des.DesEncryption(DC.dAHolder[cnt1], UTF8Encoding.UTF8.GetBytes("BuMdFerf34poKVM1".ToCharArray()), UTF8Encoding.UTF8.GetBytes("78RTrtRT".ToCharArray()));
                    AppSetting.SetAppSetting(DC.dAKeys[cnt1], tempdAHolder[cnt1]);
                }
                catch (Exception e)
                {
                    DC.ErrorLogFile(DC.ERRFILELOC, "EDcDe2002:" + e.ToString());
                    sts = false;
                }
            }
            return sts;
        }
    }
}
