﻿namespace _2CQR
{
    partial class FormSet
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.panel2 = new System.Windows.Forms.Panel();
            this.checkBox4 = new System.Windows.Forms.CheckBox();
            this.checkBox3 = new System.Windows.Forms.CheckBox();
            this.checkBox2 = new System.Windows.Forms.CheckBox();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.buttonSet = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.panelReaderSettings = new System.Windows.Forms.Panel();
            this.buttonImport = new System.Windows.Forms.Button();
            this.textBoxServerIP = new System.Windows.Forms.TextBox();
            this.labelServer0 = new System.Windows.Forms.Label();
            this.textBoxDataBaseBio = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.textBoxPassBio = new System.Windows.Forms.TextBox();
            this.textBoxSUNBio = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.panelServerSetting = new System.Windows.Forms.Panel();
            this.label26 = new System.Windows.Forms.Label();
            this.textBoxAddDBDetails = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.textBoxSyncTime = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.textBoxDisplayText = new System.Windows.Forms.TextBox();
            this.comboBoxCount = new System.Windows.Forms.ComboBox();
            this.label23 = new System.Windows.Forms.Label();
            this.textBoxQueryReport = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.comboBoxOnline = new System.Windows.Forms.ComboBox();
            this.label20 = new System.Windows.Forms.Label();
            this.textBoxBuzzerPort = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.comboBoxbUZZER = new System.Windows.Forms.ComboBox();
            this.label17 = new System.Windows.Forms.Label();
            this.textBoxSSH = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.textBoxLocation = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.comboBoxReaderType = new System.Windows.Forms.ComboBox();
            this.comboBoxDataModel = new System.Windows.Forms.ComboBox();
            this.label14 = new System.Windows.Forms.Label();
            this.comboBoxComm = new System.Windows.Forms.ComboBox();
            this.label13 = new System.Windows.Forms.Label();
            this.buttonServerSet = new System.Windows.Forms.Button();
            this.comboBoxOp = new System.Windows.Forms.ComboBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.comboBoxDelay = new System.Windows.Forms.ComboBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.textBoxReaderIP = new System.Windows.Forms.TextBox();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panelReaderSettings.SuspendLayout();
            this.panelServerSetting.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label1.Location = new System.Drawing.Point(13, 10);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "Power";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label2.Location = new System.Drawing.Point(6, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(47, 20);
            this.label2.TabIndex = 1;
            this.label2.Text = "Beep";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label3.Location = new System.Drawing.Point(6, 10);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(34, 20);
            this.label3.TabIndex = 2;
            this.label3.Text = "Ant";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.radioButton2);
            this.panel1.Controls.Add(this.radioButton1);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Location = new System.Drawing.Point(12, 42);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(260, 47);
            this.panel1.TabIndex = 3;
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.Location = new System.Drawing.Point(173, 15);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(33, 17);
            this.radioButton2.TabIndex = 3;
            this.radioButton2.TabStop = true;
            this.radioButton2.Text = "N";
            this.radioButton2.UseVisualStyleBackColor = true;
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.Location = new System.Drawing.Point(85, 16);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(32, 17);
            this.radioButton1.TabIndex = 2;
            this.radioButton1.TabStop = true;
            this.radioButton1.Text = "Y";
            this.radioButton1.UseVisualStyleBackColor = true;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.checkBox4);
            this.panel2.Controls.Add(this.checkBox3);
            this.panel2.Controls.Add(this.checkBox2);
            this.panel2.Controls.Add(this.checkBox1);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Location = new System.Drawing.Point(12, 95);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(260, 47);
            this.panel2.TabIndex = 4;
            // 
            // checkBox4
            // 
            this.checkBox4.AutoSize = true;
            this.checkBox4.Location = new System.Drawing.Point(199, 17);
            this.checkBox4.Name = "checkBox4";
            this.checkBox4.Size = new System.Drawing.Size(32, 17);
            this.checkBox4.TabIndex = 5;
            this.checkBox4.Text = "4";
            this.checkBox4.UseVisualStyleBackColor = true;
            // 
            // checkBox3
            // 
            this.checkBox3.AutoSize = true;
            this.checkBox3.Location = new System.Drawing.Point(161, 17);
            this.checkBox3.Name = "checkBox3";
            this.checkBox3.Size = new System.Drawing.Size(32, 17);
            this.checkBox3.TabIndex = 4;
            this.checkBox3.Text = "3";
            this.checkBox3.UseVisualStyleBackColor = true;
            // 
            // checkBox2
            // 
            this.checkBox2.AutoSize = true;
            this.checkBox2.Location = new System.Drawing.Point(123, 17);
            this.checkBox2.Name = "checkBox2";
            this.checkBox2.Size = new System.Drawing.Size(32, 17);
            this.checkBox2.TabIndex = 3;
            this.checkBox2.Text = "2";
            this.checkBox2.UseVisualStyleBackColor = true;
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(85, 17);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(32, 17);
            this.checkBox1.TabIndex = 2;
            this.checkBox1.Text = "1";
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "0",
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10",
            "11",
            "12",
            "13",
            "14",
            "15",
            "16",
            "17",
            "18",
            "19",
            "20",
            "21",
            "22",
            "23",
            "24",
            "25",
            "26",
            "27",
            "28",
            "29",
            "30",
            "31",
            "32",
            "33"});
            this.comboBox1.Location = new System.Drawing.Point(97, 15);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(121, 21);
            this.comboBox1.TabIndex = 5;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(224, 16);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(42, 20);
            this.label4.TabIndex = 6;
            this.label4.Text = "dBm";
            // 
            // buttonSet
            // 
            this.buttonSet.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.buttonSet.Location = new System.Drawing.Point(107, 393);
            this.buttonSet.Name = "buttonSet";
            this.buttonSet.Size = new System.Drawing.Size(75, 37);
            this.buttonSet.TabIndex = 7;
            this.buttonSet.Text = "Set";
            this.buttonSet.UseVisualStyleBackColor = true;
            this.buttonSet.Click += new System.EventHandler(this.buttonSet_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(12, 9);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(215, 26);
            this.label5.TabIndex = 8;
            this.label5.Text = "Please close and reopen the sotware for the\r\nsettings to be effective";
            // 
            // panelReaderSettings
            // 
            this.panelReaderSettings.Controls.Add(this.buttonImport);
            this.panelReaderSettings.Controls.Add(this.buttonSet);
            this.panelReaderSettings.Controls.Add(this.comboBox1);
            this.panelReaderSettings.Controls.Add(this.label4);
            this.panelReaderSettings.Controls.Add(this.label1);
            this.panelReaderSettings.Controls.Add(this.panel2);
            this.panelReaderSettings.Controls.Add(this.panel1);
            this.panelReaderSettings.Location = new System.Drawing.Point(5, 38);
            this.panelReaderSettings.Name = "panelReaderSettings";
            this.panelReaderSettings.Size = new System.Drawing.Size(278, 687);
            this.panelReaderSettings.TabIndex = 9;
            // 
            // buttonImport
            // 
            this.buttonImport.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.buttonImport.Location = new System.Drawing.Point(12, 647);
            this.buttonImport.Name = "buttonImport";
            this.buttonImport.Size = new System.Drawing.Size(75, 37);
            this.buttonImport.TabIndex = 75;
            this.buttonImport.Text = "Import";
            this.buttonImport.UseVisualStyleBackColor = true;
            this.buttonImport.Click += new System.EventHandler(this.buttonImport_Click);
            // 
            // textBoxServerIP
            // 
            this.textBoxServerIP.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxServerIP.Location = new System.Drawing.Point(104, 10);
            this.textBoxServerIP.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.textBoxServerIP.Name = "textBoxServerIP";
            this.textBoxServerIP.Size = new System.Drawing.Size(205, 26);
            this.textBoxServerIP.TabIndex = 33;
            // 
            // labelServer0
            // 
            this.labelServer0.AutoSize = true;
            this.labelServer0.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelServer0.Location = new System.Drawing.Point(9, 16);
            this.labelServer0.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelServer0.Name = "labelServer0";
            this.labelServer0.Size = new System.Drawing.Size(57, 20);
            this.labelServer0.TabIndex = 32;
            this.labelServer0.Text = "IP:Port";
            // 
            // textBoxDataBaseBio
            // 
            this.textBoxDataBaseBio.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxDataBaseBio.Location = new System.Drawing.Point(104, 105);
            this.textBoxDataBaseBio.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.textBoxDataBaseBio.Name = "textBoxDataBaseBio";
            this.textBoxDataBaseBio.Size = new System.Drawing.Size(205, 26);
            this.textBoxDataBaseBio.TabIndex = 39;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(9, 111);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(32, 20);
            this.label6.TabIndex = 38;
            this.label6.Text = "DB";
            // 
            // textBoxPassBio
            // 
            this.textBoxPassBio.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxPassBio.Location = new System.Drawing.Point(104, 74);
            this.textBoxPassBio.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.textBoxPassBio.Name = "textBoxPassBio";
            this.textBoxPassBio.Size = new System.Drawing.Size(205, 26);
            this.textBoxPassBio.TabIndex = 37;
            this.textBoxPassBio.UseSystemPasswordChar = true;
            // 
            // textBoxSUNBio
            // 
            this.textBoxSUNBio.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxSUNBio.Location = new System.Drawing.Point(104, 42);
            this.textBoxSUNBio.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.textBoxSUNBio.Name = "textBoxSUNBio";
            this.textBoxSUNBio.Size = new System.Drawing.Size(205, 26);
            this.textBoxSUNBio.TabIndex = 36;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(9, 82);
            this.label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(78, 20);
            this.label7.TabIndex = 35;
            this.label7.Text = "Password";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(9, 48);
            this.label8.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(85, 20);
            this.label8.TabIndex = 34;
            this.label8.Text = "UserName";
            // 
            // panelServerSetting
            // 
            this.panelServerSetting.Controls.Add(this.label26);
            this.panelServerSetting.Controls.Add(this.textBoxAddDBDetails);
            this.panelServerSetting.Controls.Add(this.label25);
            this.panelServerSetting.Controls.Add(this.textBoxSyncTime);
            this.panelServerSetting.Controls.Add(this.label24);
            this.panelServerSetting.Controls.Add(this.textBoxDisplayText);
            this.panelServerSetting.Controls.Add(this.comboBoxCount);
            this.panelServerSetting.Controls.Add(this.label23);
            this.panelServerSetting.Controls.Add(this.textBoxQueryReport);
            this.panelServerSetting.Controls.Add(this.label22);
            this.panelServerSetting.Controls.Add(this.label21);
            this.panelServerSetting.Controls.Add(this.comboBoxOnline);
            this.panelServerSetting.Controls.Add(this.label20);
            this.panelServerSetting.Controls.Add(this.textBoxBuzzerPort);
            this.panelServerSetting.Controls.Add(this.label19);
            this.panelServerSetting.Controls.Add(this.label18);
            this.panelServerSetting.Controls.Add(this.comboBoxbUZZER);
            this.panelServerSetting.Controls.Add(this.label17);
            this.panelServerSetting.Controls.Add(this.textBoxSSH);
            this.panelServerSetting.Controls.Add(this.label16);
            this.panelServerSetting.Controls.Add(this.textBoxLocation);
            this.panelServerSetting.Controls.Add(this.label15);
            this.panelServerSetting.Controls.Add(this.comboBoxReaderType);
            this.panelServerSetting.Controls.Add(this.comboBoxDataModel);
            this.panelServerSetting.Controls.Add(this.label14);
            this.panelServerSetting.Controls.Add(this.comboBoxComm);
            this.panelServerSetting.Controls.Add(this.label13);
            this.panelServerSetting.Controls.Add(this.buttonServerSet);
            this.panelServerSetting.Controls.Add(this.comboBoxOp);
            this.panelServerSetting.Controls.Add(this.label12);
            this.panelServerSetting.Controls.Add(this.label11);
            this.panelServerSetting.Controls.Add(this.comboBoxDelay);
            this.panelServerSetting.Controls.Add(this.label10);
            this.panelServerSetting.Controls.Add(this.label9);
            this.panelServerSetting.Controls.Add(this.textBoxPassBio);
            this.panelServerSetting.Controls.Add(this.textBoxReaderIP);
            this.panelServerSetting.Controls.Add(this.label8);
            this.panelServerSetting.Controls.Add(this.label7);
            this.panelServerSetting.Controls.Add(this.label6);
            this.panelServerSetting.Controls.Add(this.textBoxSUNBio);
            this.panelServerSetting.Controls.Add(this.textBoxDataBaseBio);
            this.panelServerSetting.Controls.Add(this.labelServer0);
            this.panelServerSetting.Controls.Add(this.textBoxServerIP);
            this.panelServerSetting.Location = new System.Drawing.Point(304, 38);
            this.panelServerSetting.Name = "panelServerSetting";
            this.panelServerSetting.Size = new System.Drawing.Size(331, 708);
            this.panelServerSetting.TabIndex = 40;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(12, 635);
            this.label26.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(87, 20);
            this.label26.TabIndex = 74;
            this.label26.Text = "AddDBDet";
            // 
            // textBoxAddDBDetails
            // 
            this.textBoxAddDBDetails.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxAddDBDetails.Location = new System.Drawing.Point(106, 632);
            this.textBoxAddDBDetails.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.textBoxAddDBDetails.Name = "textBoxAddDBDetails";
            this.textBoxAddDBDetails.Size = new System.Drawing.Size(175, 26);
            this.textBoxAddDBDetails.TabIndex = 73;
            this.textBoxAddDBDetails.UseSystemPasswordChar = true;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(12, 606);
            this.label25.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(78, 20);
            this.label25.TabIndex = 72;
            this.label25.Text = "SyncTime";
            // 
            // textBoxSyncTime
            // 
            this.textBoxSyncTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxSyncTime.Location = new System.Drawing.Point(106, 600);
            this.textBoxSyncTime.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.textBoxSyncTime.Name = "textBoxSyncTime";
            this.textBoxSyncTime.Size = new System.Drawing.Size(175, 26);
            this.textBoxSyncTime.TabIndex = 71;
            this.textBoxSyncTime.UseSystemPasswordChar = true;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(12, 574);
            this.label24.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(90, 20);
            this.label24.TabIndex = 70;
            this.label24.Text = "DisplayText";
            // 
            // textBoxDisplayText
            // 
            this.textBoxDisplayText.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxDisplayText.Location = new System.Drawing.Point(106, 568);
            this.textBoxDisplayText.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.textBoxDisplayText.Name = "textBoxDisplayText";
            this.textBoxDisplayText.Size = new System.Drawing.Size(175, 26);
            this.textBoxDisplayText.TabIndex = 69;
            this.textBoxDisplayText.UseSystemPasswordChar = true;
            // 
            // comboBoxCount
            // 
            this.comboBoxCount.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxCount.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxCount.FormattingEnabled = true;
            this.comboBoxCount.Items.AddRange(new object[] {
            "No",
            "Yes"});
            this.comboBoxCount.Location = new System.Drawing.Point(104, 534);
            this.comboBoxCount.Name = "comboBoxCount";
            this.comboBoxCount.Size = new System.Drawing.Size(175, 28);
            this.comboBoxCount.TabIndex = 68;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(9, 537);
            this.label23.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(74, 20);
            this.label23.TabIndex = 67;
            this.label23.Text = "CountDP";
            // 
            // textBoxQueryReport
            // 
            this.textBoxQueryReport.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxQueryReport.Location = new System.Drawing.Point(104, 502);
            this.textBoxQueryReport.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.textBoxQueryReport.Name = "textBoxQueryReport";
            this.textBoxQueryReport.Size = new System.Drawing.Size(175, 26);
            this.textBoxQueryReport.TabIndex = 66;
            this.textBoxQueryReport.UseSystemPasswordChar = true;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(6, 508);
            this.label22.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(70, 20);
            this.label22.TabIndex = 65;
            this.label22.Text = "ReportQ";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(289, 471);
            this.label21.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(17, 20);
            this.label21.TabIndex = 64;
            this.label21.Text = "s";
            // 
            // comboBoxOnline
            // 
            this.comboBoxOnline.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxOnline.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxOnline.FormattingEnabled = true;
            this.comboBoxOnline.Items.AddRange(new object[] {
            "10",
            "30",
            "60",
            "300",
            "900",
            "1800",
            "3600"});
            this.comboBoxOnline.Location = new System.Drawing.Point(106, 468);
            this.comboBoxOnline.Name = "comboBoxOnline";
            this.comboBoxOnline.Size = new System.Drawing.Size(175, 28);
            this.comboBoxOnline.TabIndex = 63;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(6, 471);
            this.label20.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(54, 20);
            this.label20.TabIndex = 62;
            this.label20.Text = "Online";
            // 
            // textBoxBuzzerPort
            // 
            this.textBoxBuzzerPort.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxBuzzerPort.Location = new System.Drawing.Point(106, 436);
            this.textBoxBuzzerPort.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.textBoxBuzzerPort.Name = "textBoxBuzzerPort";
            this.textBoxBuzzerPort.Size = new System.Drawing.Size(175, 26);
            this.textBoxBuzzerPort.TabIndex = 61;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(4, 442);
            this.label19.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(88, 20);
            this.label19.TabIndex = 59;
            this.label19.Text = "BuzzerPort";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(6, 408);
            this.label18.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(59, 20);
            this.label18.TabIndex = 57;
            this.label18.Text = "Buzzer";
            // 
            // comboBoxbUZZER
            // 
            this.comboBoxbUZZER.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxbUZZER.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxbUZZER.FormattingEnabled = true;
            this.comboBoxbUZZER.Items.AddRange(new object[] {
            "No",
            "2CQR"});
            this.comboBoxbUZZER.Location = new System.Drawing.Point(106, 402);
            this.comboBoxbUZZER.Name = "comboBoxbUZZER";
            this.comboBoxbUZZER.Size = new System.Drawing.Size(175, 28);
            this.comboBoxbUZZER.TabIndex = 58;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(12, 175);
            this.label17.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(79, 20);
            this.label17.TabIndex = 55;
            this.label17.Text = "SSH(U,P)";
            // 
            // textBoxSSH
            // 
            this.textBoxSSH.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxSSH.Location = new System.Drawing.Point(104, 169);
            this.textBoxSSH.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.textBoxSSH.Name = "textBoxSSH";
            this.textBoxSSH.Size = new System.Drawing.Size(205, 26);
            this.textBoxSSH.TabIndex = 56;
            this.textBoxSSH.UseSystemPasswordChar = true;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(9, 143);
            this.label16.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(70, 20);
            this.label16.TabIndex = 53;
            this.label16.Text = "Location";
            // 
            // textBoxLocation
            // 
            this.textBoxLocation.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxLocation.Location = new System.Drawing.Point(104, 137);
            this.textBoxLocation.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.textBoxLocation.Name = "textBoxLocation";
            this.textBoxLocation.Size = new System.Drawing.Size(205, 26);
            this.textBoxLocation.TabIndex = 54;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(6, 375);
            this.label15.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(96, 20);
            this.label15.TabIndex = 51;
            this.label15.Text = "ReaderType";
            // 
            // comboBoxReaderType
            // 
            this.comboBoxReaderType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxReaderType.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxReaderType.FormattingEnabled = true;
            this.comboBoxReaderType.Items.AddRange(new object[] {
            "C001",
            "C002",
            "C003",
            "C006",
            "B001",
            "B002",
            "B003",
            "H003",
            "2C003",
            "C002B001",
            "D001",
            "A0010",
            "_4A001",
            "StockReader",
            "Demo"});
            this.comboBoxReaderType.Location = new System.Drawing.Point(106, 369);
            this.comboBoxReaderType.Name = "comboBoxReaderType";
            this.comboBoxReaderType.Size = new System.Drawing.Size(175, 28);
            this.comboBoxReaderType.TabIndex = 52;
            // 
            // comboBoxDataModel
            // 
            this.comboBoxDataModel.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxDataModel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxDataModel.FormattingEnabled = true;
            this.comboBoxDataModel.Items.AddRange(new object[] {
            "Default",
            "ASCII",
            "ASCIISV",
            "ISO28560",
            "UOKDm"});
            this.comboBoxDataModel.Location = new System.Drawing.Point(106, 335);
            this.comboBoxDataModel.Name = "comboBoxDataModel";
            this.comboBoxDataModel.Size = new System.Drawing.Size(175, 28);
            this.comboBoxDataModel.TabIndex = 50;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(12, 342);
            this.label14.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(87, 20);
            this.label14.TabIndex = 49;
            this.label14.Text = "DataModel";
            // 
            // comboBoxComm
            // 
            this.comboBoxComm.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxComm.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxComm.FormattingEnabled = true;
            this.comboBoxComm.Items.AddRange(new object[] {
            "Demo",
            "RaDB",
            "SuDB",
            "SumSafeTy",
            "SVWs",
            "SDB",
            "SCFIn",
            "SCFOut",
            "SCI",
            "SCO",
            "SCF",
            "SRPH",
            "SCWhHo",
            "UOK",
            "_2CQRWebLog"});
            this.comboBoxComm.Location = new System.Drawing.Point(106, 301);
            this.comboBoxComm.Name = "comboBoxComm";
            this.comboBoxComm.Size = new System.Drawing.Size(175, 28);
            this.comboBoxComm.TabIndex = 48;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(12, 308);
            this.label13.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(55, 20);
            this.label13.TabIndex = 47;
            this.label13.Text = "Comm";
            // 
            // buttonServerSet
            // 
            this.buttonServerSet.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.buttonServerSet.Location = new System.Drawing.Point(136, 662);
            this.buttonServerSet.Name = "buttonServerSet";
            this.buttonServerSet.Size = new System.Drawing.Size(75, 37);
            this.buttonServerSet.TabIndex = 8;
            this.buttonServerSet.Text = "Set";
            this.buttonServerSet.UseVisualStyleBackColor = true;
            this.buttonServerSet.Click += new System.EventHandler(this.buttonServerSet_Click);
            // 
            // comboBoxOp
            // 
            this.comboBoxOp.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxOp.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxOp.FormattingEnabled = true;
            this.comboBoxOp.Items.AddRange(new object[] {
            "Cursor",
            "DB",
            "API"});
            this.comboBoxOp.Location = new System.Drawing.Point(106, 267);
            this.comboBoxOp.Name = "comboBoxOp";
            this.comboBoxOp.Size = new System.Drawing.Size(175, 28);
            this.comboBoxOp.TabIndex = 46;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(12, 273);
            this.label12.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(58, 20);
            this.label12.TabIndex = 45;
            this.label12.Text = "Output";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(289, 239);
            this.label11.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(17, 20);
            this.label11.TabIndex = 44;
            this.label11.Text = "s";
            // 
            // comboBoxDelay
            // 
            this.comboBoxDelay.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxDelay.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxDelay.FormattingEnabled = true;
            this.comboBoxDelay.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10",
            "15",
            "20",
            "25",
            "30",
            "35",
            "50",
            "100",
            "300",
            "600",
            "900",
            "1800",
            "3600"});
            this.comboBoxDelay.Location = new System.Drawing.Point(106, 233);
            this.comboBoxDelay.Name = "comboBoxDelay";
            this.comboBoxDelay.Size = new System.Drawing.Size(175, 28);
            this.comboBoxDelay.TabIndex = 43;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(12, 237);
            this.label10.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(49, 20);
            this.label10.TabIndex = 42;
            this.label10.Text = "Delay";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(12, 207);
            this.label9.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(76, 20);
            this.label9.TabIndex = 40;
            this.label9.Text = "ReaderIp";
            // 
            // textBoxReaderIP
            // 
            this.textBoxReaderIP.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxReaderIP.Location = new System.Drawing.Point(106, 201);
            this.textBoxReaderIP.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.textBoxReaderIP.Name = "textBoxReaderIP";
            this.textBoxReaderIP.Size = new System.Drawing.Size(205, 26);
            this.textBoxReaderIP.TabIndex = 41;
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            this.openFileDialog1.FileOk += new System.ComponentModel.CancelEventHandler(this.openFileDialog1_FileOk);
            // 
            // FormSet
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(651, 749);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.panelReaderSettings);
            this.Controls.Add(this.panelServerSetting);
            this.Name = "FormSet";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FormSetv1.42";
            this.TopMost = true;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormSet_FormClosing);
            this.Load += new System.EventHandler(this.FormSet_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panelReaderSettings.ResumeLayout(false);
            this.panelReaderSettings.PerformLayout();
            this.panelServerSetting.ResumeLayout(false);
            this.panelServerSetting.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.RadioButton radioButton1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.CheckBox checkBox4;
        private System.Windows.Forms.CheckBox checkBox3;
        private System.Windows.Forms.CheckBox checkBox2;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button buttonSet;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Panel panelReaderSettings;
        private System.Windows.Forms.TextBox textBoxServerIP;
        private System.Windows.Forms.Label labelServer0;
        private System.Windows.Forms.TextBox textBoxDataBaseBio;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox textBoxPassBio;
        private System.Windows.Forms.TextBox textBoxSUNBio;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Panel panelServerSetting;
        private System.Windows.Forms.Button buttonServerSet;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox textBoxReaderIP;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox comboBoxDelay;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ComboBox comboBoxOp;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.ComboBox comboBoxComm;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.ComboBox comboBoxDataModel;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.ComboBox comboBoxReaderType;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox textBoxLocation;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox textBoxSSH;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.ComboBox comboBoxbUZZER;
        private System.Windows.Forms.TextBox textBoxBuzzerPort;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.ComboBox comboBoxOnline;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox textBoxQueryReport;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.ComboBox comboBoxCount;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox textBoxDisplayText;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox textBoxSyncTime;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.TextBox textBoxAddDBDetails;
        private System.Windows.Forms.Button buttonImport;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
    }
}