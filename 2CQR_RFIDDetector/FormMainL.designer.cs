﻿namespace _2CQR
{
    partial class FormMainL
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormMainL));
            this.labelTitle = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.radioButtonDefault = new System.Windows.Forms.RadioButton();
            this.buttonoK = new System.Windows.Forms.Button();
            this.radioButtonWeighing = new System.Windows.Forms.RadioButton();
            this.radioButtonReport = new System.Windows.Forms.RadioButton();
            this.radioButtonDeactivate = new System.Windows.Forms.RadioButton();
            this.SuspendLayout();
            // 
            // labelTitle
            // 
            this.labelTitle.BackColor = System.Drawing.Color.Transparent;
            this.labelTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTitle.Location = new System.Drawing.Point(4, 60);
            this.labelTitle.Name = "labelTitle";
            this.labelTitle.Size = new System.Drawing.Size(280, 56);
            this.labelTitle.TabIndex = 0;
            this.labelTitle.Text = "Please Select The Mode";
            this.labelTitle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(12, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(74, 25);
            this.label2.TabIndex = 1;
            this.label2.Text = "2CQR";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Location = new System.Drawing.Point(1, 34);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(283, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "---------------------------------------------------------------------------------" +
    "-----------";
            // 
            // radioButtonDefault
            // 
            this.radioButtonDefault.BackColor = System.Drawing.Color.Transparent;
            this.radioButtonDefault.CheckAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.radioButtonDefault.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButtonDefault.Location = new System.Drawing.Point(4, 133);
            this.radioButtonDefault.Name = "radioButtonDefault";
            this.radioButtonDefault.Size = new System.Drawing.Size(261, 31);
            this.radioButtonDefault.TabIndex = 5;
            this.radioButtonDefault.Text = "Default";
            this.radioButtonDefault.UseVisualStyleBackColor = false;
            // 
            // buttonoK
            // 
            this.buttonoK.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonoK.Location = new System.Drawing.Point(100, 331);
            this.buttonoK.Name = "buttonoK";
            this.buttonoK.Size = new System.Drawing.Size(75, 42);
            this.buttonoK.TabIndex = 7;
            this.buttonoK.Text = "OK";
            this.buttonoK.UseVisualStyleBackColor = true;
            this.buttonoK.Click += new System.EventHandler(this.buttonoK_Click);
            // 
            // radioButtonWeighing
            // 
            this.radioButtonWeighing.BackColor = System.Drawing.Color.Transparent;
            this.radioButtonWeighing.CheckAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.radioButtonWeighing.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButtonWeighing.Location = new System.Drawing.Point(4, 170);
            this.radioButtonWeighing.Name = "radioButtonWeighing";
            this.radioButtonWeighing.Size = new System.Drawing.Size(261, 31);
            this.radioButtonWeighing.TabIndex = 8;
            this.radioButtonWeighing.Text = "Weighing";
            this.radioButtonWeighing.UseVisualStyleBackColor = false;
            // 
            // radioButtonReport
            // 
            this.radioButtonReport.BackColor = System.Drawing.Color.Transparent;
            this.radioButtonReport.CheckAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.radioButtonReport.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButtonReport.Location = new System.Drawing.Point(4, 207);
            this.radioButtonReport.Name = "radioButtonReport";
            this.radioButtonReport.Size = new System.Drawing.Size(261, 31);
            this.radioButtonReport.TabIndex = 9;
            this.radioButtonReport.Text = "Report";
            this.radioButtonReport.UseVisualStyleBackColor = false;
            // 
            // radioButtonDeactivate
            // 
            this.radioButtonDeactivate.BackColor = System.Drawing.Color.Transparent;
            this.radioButtonDeactivate.CheckAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.radioButtonDeactivate.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButtonDeactivate.Location = new System.Drawing.Point(9, 244);
            this.radioButtonDeactivate.Name = "radioButtonDeactivate";
            this.radioButtonDeactivate.Size = new System.Drawing.Size(261, 31);
            this.radioButtonDeactivate.TabIndex = 10;
            this.radioButtonDeactivate.Text = "Deactivate";
            this.radioButtonDeactivate.UseVisualStyleBackColor = false;
            // 
            // FormMainL
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 385);
            this.Controls.Add(this.radioButtonDeactivate);
            this.Controls.Add(this.radioButtonReport);
            this.Controls.Add(this.radioButtonWeighing);
            this.Controls.Add(this.buttonoK);
            this.Controls.Add(this.radioButtonDefault);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.labelTitle);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FormMainL";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Select Reader Type";
            this.TopMost = true;
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelTitle;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.RadioButton radioButtonDefault;
        private System.Windows.Forms.Button buttonoK;
        private System.Windows.Forms.RadioButton radioButtonWeighing;
        private System.Windows.Forms.RadioButton radioButtonReport;
        private System.Windows.Forms.RadioButton radioButtonDeactivate;
    }
}