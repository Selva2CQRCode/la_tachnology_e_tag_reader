﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _2CQR
{
    public partial class FormSet : Form
    {
        public FormSet()
        {
            InitializeComponent();
        }

        private void FormSet_Load(object sender, EventArgs e)
        {
            if (DC.readerTypeSel.getSettings())
            {
                this.comboBox1.Text = DC.setPara.powerDbm.ToString();
                if (DC.setPara.beep == 1)
                    this.radioButton1.Checked = true;
                else
                    this.radioButton2.Checked = true;
                if ((DC.setPara.antenna & 0x01) == 0x01)
                    this.checkBox1.Checked = true;
                if ((DC.setPara.antenna & 0x02) == 0x02)
                    this.checkBox2.Checked = true;
                if ((DC.setPara.antenna & 0x04) == 0x04)
                    this.checkBox3.Checked = true;
                if ((DC.setPara.antenna & 0x08) == 0x08)
                    this.checkBox4.Checked = true;
            }
            else
            {
                this.panelReaderSettings.Enabled = false;
                MessageBox.Show("GetSettings Failed");
            }

            this.textBoxServerIP.Text = DC.dAHolder[(int)DC.dAName.SIP];
            this.textBoxSUNBio.Text = DC.dAHolder[(int)DC.dAName.SUN];
            this.textBoxPassBio.Text = DC.dAHolder[(int)DC.dAName.Pass];
            this.textBoxDataBaseBio.Text = DC.dAHolder[(int)DC.dAName.DB];
            this.textBoxReaderIP.Text = DC.dAHolder[(int)DC.dAName.ReaderIP];
            this.comboBoxDelay.Text = DC.dAHolder[(int)DC.dAName.Delay];
            this.comboBoxOp.Text = DC.dAHolder[(int)DC.dAName.Output];
            this.comboBoxComm.Text = DC.dAHolder[(int)DC.dAName.Comm];
            this.comboBoxDataModel.Text = DC.dAHolder[(int)DC.dAName.DataModel];
            this.comboBoxReaderType.Text = DC.dAHolder[(int)DC.dAName.ReaderTypeN];
            this.textBoxLocation.Text = DC.dAHolder[(int)DC.dAName.Location];
            this.textBoxSSH.Text = DC.dAHolder[(int)DC.dAName.SSH];
            this.comboBoxbUZZER.Text = DC.dAHolder[(int)DC.dAName.Buzzer];
            this.textBoxBuzzerPort.Text = DC.dAHolder[(int)DC.dAName.EMComPortNo];
            this.comboBoxOnline.Text = DC.dAHolder[(int)DC.dAName.ReaderOnlineTime];
            this.textBoxQueryReport.Text = DC.dAHolder[(int)DC.dAName.QRep];
            this.comboBoxCount.Text = DC.dAHolder[(int)DC.dAName.CountDP];
            this.textBoxDisplayText.Text = DC.dAHolder[(int)DC.dAName.DisplayText];
            this.textBoxSyncTime.Text = DC.dAHolder[(int)DC.dAName.Synctime];
            this.textBoxAddDBDetails.Text = DC.dAHolder[(int)DC.dAName.AddDBDetails];
        }

        private void buttonSet_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            DC.setPara.powerDbm = (byte)this.comboBox1.SelectedIndex;
            DC.setPara.beep = this.radioButton1.Checked == true ? (byte)1 : (byte)0;
            byte ANT = 0;
            if (checkBox1.Checked) ANT = Convert.ToByte(ANT | 1);
            if (checkBox2.Checked) ANT = Convert.ToByte(ANT | 2);
            if (checkBox3.Checked) ANT = Convert.ToByte(ANT | 4);
            if (checkBox4.Checked) ANT = Convert.ToByte(ANT | 8);
            DC.setPara.antenna = ANT;
            DC.dAHolder[(int)DC.dAName.C6Ant] = ANT.ToString();
            DES des = new DES();
            string tempdAHolder = des.DesEncryption(DC.dAHolder[(int)DC.dAName.C6Ant], UTF8Encoding.UTF8.GetBytes("BuMdFerf34poKVM1".ToCharArray()), UTF8Encoding.UTF8.GetBytes("78RTrtRT".ToCharArray()));
            AppSetting.SetAppSetting(DC.dAKeys[(int)DC.dAName.C6Ant], tempdAHolder);
            DC.readerTypeSel.setSettings();
            if (DC.readerTypeSel.getSettings())
            {
                this.checkBox1.Checked = false;
                this.checkBox2.Checked = false;
                this.checkBox3.Checked = false;
                this.checkBox4.Checked = false;
                this.comboBox1.Text = DC.setPara.powerDbm.ToString();
                if (DC.setPara.beep == 1)
                    this.radioButton1.Checked = true;
                else
                    this.radioButton2.Checked = true;
                if ((DC.setPara.antenna & 0x01) == 0x01)
                    this.checkBox1.Checked = true;
                if ((DC.setPara.antenna & 0x02) == 0x02)
                    this.checkBox2.Checked = true;
                if ((DC.setPara.antenna & 0x04) == 0x04)
                    this.checkBox3.Checked = true;
                if ((DC.setPara.antenna & 0x08) == 0x08)
                    this.checkBox4.Checked = true;
            }
            Cursor.Current = Cursors.Default;
        }

        private void buttonServerSet_Click(object sender, EventArgs e)
        {
            DC.dAHolder[(int)DC.dAName.SIP] = this.textBoxServerIP.Text;
            DC.dAHolder[(int)DC.dAName.SUN] = this.textBoxSUNBio.Text;
            DC.dAHolder[(int)DC.dAName.Pass] = this.textBoxPassBio.Text;
            DC.dAHolder[(int)DC.dAName.DB] = this.textBoxDataBaseBio.Text;
            DC.dAHolder[(int)DC.dAName.ReaderIP] = this.textBoxReaderIP.Text;
            DC.dAHolder[(int)DC.dAName.Delay] = this.comboBoxDelay.Text;
            DC.dAHolder[(int)DC.dAName.Output] = this.comboBoxOp.Text;
            DC.dAHolder[(int)DC.dAName.Comm] = this.comboBoxComm.Text;
            DC.dAHolder[(int)DC.dAName.DataModel] = this.comboBoxDataModel.Text;
            DC.dAHolder[(int)DC.dAName.ReaderTypeN] = this.comboBoxReaderType.Text;
            DC.dAHolder[(int)DC.dAName.Location] = this.textBoxLocation.Text;
            DC.dAHolder[(int)DC.dAName.SSH] = this.textBoxSSH.Text;
            DC.dAHolder[(int)DC.dAName.Buzzer] = this.comboBoxbUZZER.Text;
            DC.dAHolder[(int)DC.dAName.EMComPortNo] = this.textBoxBuzzerPort.Text;
            DC.dAHolder[(int)DC.dAName.ReaderOnlineTime] = this.comboBoxOnline.Text;
            DC.dAHolder[(int)DC.dAName.QRep] = this.textBoxQueryReport.Text;
            DC.dAHolder[(int)DC.dAName.CountDP] = this.comboBoxCount.Text;
            DC.dAHolder[(int)DC.dAName.DisplayText] = this.textBoxDisplayText.Text;
            DC.dAHolder[(int)DC.dAName.Synctime] = this.textBoxSyncTime.Text;
            DC.dAHolder[(int)DC.dAName.AddDBDetails] = this.textBoxAddDBDetails.Text;

            DES des = new DES();
            string[] tempdAHolder = new string[DC.dAValue.Length];
            // For Iterating the loop
            int cnt1 = 0;
            // To save the settings made into Storage
            for (cnt1 = 0; cnt1 < DC.dAValue.Length; cnt1++)
            {
                tempdAHolder[cnt1] = des.DesEncryption(DC.dAHolder[cnt1], UTF8Encoding.UTF8.GetBytes("BuMdFerf34poKVM1".ToCharArray()), UTF8Encoding.UTF8.GetBytes("78RTrtRT".ToCharArray()));
                AppSetting.SetAppSetting(DC.dAKeys[cnt1], tempdAHolder[cnt1]);
            }
        }

        private void FormSet_FormClosing(object sender, FormClosingEventArgs e)
        {
            
        }

        private void buttonImport_Click(object sender, EventArgs e)
        {
            if (DC.dAHolder[(int)DC.dAName.Comm] == "SRPH")
            {
                MessageBox.Show("Please do batch upload(max  in the format:MEMBERID,CSN,MEMBERTYPE: as .csv file");
                this.openFileDialog1.ShowDialog();
            }
            else
            {
                MessageBox.Show("Not Valid");
            }
        }

        private void openFileDialog1_FileOk(object sender, CancelEventArgs e)
        {
                DC.ErrorLogFile(DC.ERRFILELOC, openFileDialog1.FileName);
               MessageBox.Show( DC.connTypeSel.importData(this.openFileDialog1.FileName));
        }
    }
}
