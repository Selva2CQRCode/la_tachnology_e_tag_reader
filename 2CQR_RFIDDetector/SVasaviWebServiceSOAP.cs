﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Renci.SshNet;
using System.Net;
using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Xml;

namespace _2CQR
{
    public class SVasaviWebServiceSOAP : InterConnectivity
    {
        HttpWebRequest httpWebRequest;
        private static bool soapConnect = true;
        private static int sno = 0;
        public string importData(string filename)
        {
            return "";
        }
        public bool updateSync(string content)
        {
            return true;
        }
        public bool postSyncTotalData()
        {
            return true;
        }

        public void ClosingComm()
        {
        }

        public bool OnlineStsCheck()
        {
            return soapConnect;
        }

        public bool ServerConnection()
        {
            return soapConnect;
        }

        public string getUpdateContent(string Item)
        {
            sno++;
            string[] tempId = Item.Split(',');
            string itemIdFormat = DC.dataModelTypeSel.getItemId(tempId[0]);
            return (sno.ToString() + ";" + itemIdFormat + ";" + tempId[1] + ";" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
        }
        public bool update(string content)
        {
            try
            {

                string answer = "";
                DC.ErrorLogFile(DC.ERRFILELOC, "Server:" + content + ":");
                XmlDocument soapEnvelopeXml = CreateSoapEnvelope(content);
                createConnection("GetTag");
                InsertSoapEnvelopeIntoWebRequest(soapEnvelopeXml, httpWebRequest);

                // begin async call to web request.
                IAsyncResult asyncResult = httpWebRequest.BeginGetResponse(null, null);

                // suspend this thread until call is complete. You might want to
                // do something usefull here like update your UI.
                asyncResult.AsyncWaitHandle.WaitOne();

                // get the response from the completed web request.
                XmlReader soapResult;

                XmlReaderSettings xmlSettings = new XmlReaderSettings();
                xmlSettings.XmlResolver = (XmlResolver)null;
                xmlSettings.IgnoreComments = true;
                xmlSettings.IgnoreWhitespace = true;
                xmlSettings.DtdProcessing = DtdProcessing.Parse;
                using (WebResponse webResponse = httpWebRequest.EndGetResponse(asyncResult))
                {
                    using (StreamReader rd = new StreamReader(webResponse.GetResponseStream()))
                    {
                        soapResult = XmlReader.Create(rd, xmlSettings);
                        while (soapResult.Read())
                        {
                            if (soapResult.Name.Equals("GetTagResult"))
                            {
                                answer = soapResult.ReadString();
                            }
                        }
                    }
                }
                soapConnect = true;
                if (answer.Contains("Success"))
                {
                    return true;
                }
                else
                {
                    DC.ErrorLogFile(DC.ERRFILELOC, "ESVWS0001:" + answer.ToString());
                    return false;
                }
            }

            catch (Exception ex)
            {
                DC.ErrorLogFile(DC.ERRFILELOC, "ESVWS0002:" + ex.ToString());
                soapConnect = false;
                return false;
            }
        }

        public bool checkNeeded(string epc)
        {
            return true;
        }

        public bool Report(string query)
        {
            return true;
        }

        public bool createConnection(string header)
        {
            try
            {
                //Creates an HttpWebRequest for the specified URL.
                // Expected link
                //http://103.48.183.155/WSTagCheck.asmx?op=
                httpWebRequest = (HttpWebRequest)WebRequest.Create(DC.dAHolder[(int)DC.dAName.SIP] + header);
                //Set HttpWebRequest properties
                //  httpWebRequest.Headers.Add("SOAPAction", "http://103.48.183.155/WSTagCheck.asmx?op=");
                httpWebRequest.Method = "POST";
                httpWebRequest.Timeout = 20000;
                httpWebRequest.KeepAlive = true;
                httpWebRequest.ContentType = "text/xml;charset=\"utf-8\"";
                httpWebRequest.Accept = "text/xml";
                return true;
            }
            catch (Exception e)
            {
                // DC.errMsg = "Cannot create instance";
                 DC.ErrorLogFile(DC.ERRFILELOC, "ECoNC1018:" + e.ToString());
                return false;
            }
        }


        private XmlDocument CreateSoapEnvelope(string content)
        {
            XmlDocument soapEnvelopeDocument = new XmlDocument();
            soapEnvelopeDocument.LoadXml(
          @"<?xml version=""1.0"" encoding =""utf-8""?> 
             <soap:Envelope xmlns:soap=""http://schemas.xmlsoap.org/soap/envelope/"" 
               xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" 
               xmlns:xsd=""http://www.w3.org/2001/XMLSchema"">
        <soap:Body>
            <GetTag xmlns=""http://tempuri.org/"" >
<Login>" + DC.dAHolder[(int)DC.dAName.SUN] + @"</Login>
<Password>" + DC.dAHolder[(int)DC.dAName.Pass] + @"</Password>
                <sTag>" + content + @"</sTag>
            </GetTag>
        </soap:Body>
    </soap:Envelope>");
            return soapEnvelopeDocument;
        }

        private XmlDocument CreateSoapEnvelopeGetRFIDTag(string content)
        {
            XmlDocument soapEnvelopeDocument = new XmlDocument();
            soapEnvelopeDocument.LoadXml(
          @"<?xml version=""1.0"" encoding =""utf-8""?> 
             <soap:Envelope xmlns:soap=""http://schemas.xmlsoap.org/soap/envelope/"" 
               xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" 
               xmlns:xsd=""http://www.w3.org/2001/XMLSchema"">
        <soap:Body>
            <GetRFIDTag xmlns=""http://tempuri.org/"" >
<Login>" + DC.dAHolder[(int)DC.dAName.SUN] + @"</Login>
<Password>" + DC.dAHolder[(int)DC.dAName.Pass] + @"</Password>
                <sRFIDTag>" + content + @"</sRFIDTag>
            </GetRFIDTag>
        </soap:Body>
    </soap:Envelope>");
            return soapEnvelopeDocument;
        }

        private static void InsertSoapEnvelopeIntoWebRequest(XmlDocument soapEnvelopeXml, HttpWebRequest webRequest)
        {
            using (Stream stream = webRequest.GetRequestStream())
            {
                soapEnvelopeXml.Save(stream);
            }
        }

        public string getRFIDTagData(string content)
        {
            XmlDocument soapEnvelopeXml = CreateSoapEnvelopeGetRFIDTag(content);
            createConnection("GetRFIDTag");
            InsertSoapEnvelopeIntoWebRequest(soapEnvelopeXml, httpWebRequest);

            // begin async call to web request.
            IAsyncResult asyncResult = httpWebRequest.BeginGetResponse(null, null);

            // suspend this thread until call is complete. You might want to
            // do something usefull here like update your UI.
            asyncResult.AsyncWaitHandle.WaitOne();

            // get the response from the completed web request.
            XmlReader soapResult;

            XmlReaderSettings xmlSettings = new XmlReaderSettings();
            xmlSettings.XmlResolver = (XmlResolver)null;
            xmlSettings.IgnoreComments = true;
            xmlSettings.IgnoreWhitespace = true;
            xmlSettings.DtdProcessing = DtdProcessing.Parse;
            using (WebResponse webResponse = httpWebRequest.EndGetResponse(asyncResult))
            {
                using (StreamReader rd = new StreamReader(webResponse.GetResponseStream()))
                {
                    soapResult = XmlReader.Create(rd, xmlSettings);
                    while (soapResult.Read())
                    {
                        if (soapResult.Name.Equals("GetTagResult"))
                        {
                           string answer = soapResult.ReadString();
                        }
                    }
                }
            }
            soapConnect = true;
            return "";
        }


    }
}
