﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace _2CQR
{
    public partial class FormMainL : Form
    {
        public FormMainL()
        {
            InitializeComponent();
            DoItFirst();
        }

        public void DoItFirst()
        {
            try
            {
                LoadImages();
            }
            catch(Exception e)
            {
                DC.ErrorLogFile(DC.ERRFILELOC, "FoReCh1001:" + e.ToString());
            }
        }

        public void LoadImages()
        {
           
        }

        private void buttonoK_Click(object sender, EventArgs e)
        {
            if (this.radioButtonDefault.Checked == true)
                DC.dAHolder[(int)DC.dAName.Mode] = "Default";
            else if (this.radioButtonWeighing.Checked == true)
                DC.dAHolder[(int)DC.dAName.Mode] = "Weighing";
            else if (this.radioButtonReport.Checked == true)
                DC.dAHolder[(int)DC.dAName.Mode] = "Report";
            else if (this.radioButtonDeactivate.Checked == true)
                DC.dAHolder[(int)DC.dAName.Mode] = "Deactivate";
            else
                DC.dAHolder[(int)DC.dAName.Mode] = "Default";
            new DESLoadStore().StoreConfiguration();
            this.Close();
        }

        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams cp = base.CreateParams;
                cp.ExStyle |= 0x02000000;  // Turn on WS_EX_COMPOSITED
                return cp;
            }
        }
    }
}
