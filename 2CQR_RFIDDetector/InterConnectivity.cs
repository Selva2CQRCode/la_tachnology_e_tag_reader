﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _2CQR
{
    public interface InterConnectivity
    {
        bool ServerConnection();
        bool OnlineStsCheck();

        bool update(string content);
        void ClosingComm();

        bool checkNeeded(string epc);

        bool Report(string query);

        bool postSyncTotalData();


        string getRFIDTagData(string content);
        string importData(string filename);

        //  string getUpdateContent();
    }
}
